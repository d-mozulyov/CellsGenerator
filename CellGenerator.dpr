program CellGenerator;

{$APPTYPE CONSOLE}

// compiler options
{$if CompilerVersion >= 24}
  {$LEGACYIFEND ON}
{$ifend}
{$if CompilerVersion >= 23}
  {$define UNITSCOPENAMES}
{$ifend}
{$U-}{$V+}{$B-}{$X+}{$T+}{$P+}{$H+}{$J-}{$Z1}{$A4}
{$ifndef VER140}
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$endif}
{$O+}{$R-}{$I-}{$Q-}{$W-}

uses
  {$ifdef UNITSCOPENAMES}
  Winapi.Windows, System.SysUtils, System.Classes,
  {$else}
  Windows, SysUtils, Classes,
  {$endif}
  DocumentData in 'DocumentData.pas',
  Generators in 'Generators.pas',
  XmlGenerators in 'Generators\XmlGenerators.pas';


// native ordinal types
{$if (not Defined(FPC)) and (CompilerVersion < 22)}
type
  {$if CompilerVersion < 21}
  NativeInt = Integer;
  NativeUInt = Cardinal;
  {$ifend}
  PNativeInt = ^NativeInt;
  PNativeUInt = ^NativeUInt;
{$ifend}


procedure Generate(const GeneratorClass: TGeneratorClass; const FileName: string);
var
  Instance: TGenerator;
begin
  Write(GeneratorClass.ClassName, ' --> "', ExtractFileName(FileName), '"...');

  Instance := GeneratorClass.Create(FileName);
  try
    Instance.Run;
  finally
    Instance.Free;
  end;

  Writeln(' done.');
end;

begin
  try
    Generate(TXmlGenerator, 'XmlGenerator.xml');
    Generate(TCompressedXmlGenerator, 'XmlGenerator.zlibxml');
  except
    on E: Exception do
    Writeln(E.ClassName, ': ', E.Message);
  end;

  if (ParamStr(1) <> '-nowait') then
  begin
    Writeln;
    Write('Press Enter to quit');
    Readln;
  end;
end.
