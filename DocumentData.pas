unit DocumentData;

// compiler options
{$if CompilerVersion >= 24}
  {$LEGACYIFEND ON}
{$ifend}
{$if CompilerVersion >= 23}
  {$define UNITSCOPENAMES}
{$ifend}
{$U-}{$V+}{$B-}{$X+}{$T+}{$P+}{$H+}{$J-}{$Z1}{$A4}
{$ifndef VER140}
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$endif}
{$O+}{$R-}{$I-}{$Q-}{$W-}

interface

// native ordinal types
{$if (not Defined(FPC)) and (CompilerVersion < 22)}
type
  {$if CompilerVersion < 21}
  NativeInt = Integer;
  NativeUInt = Cardinal;
  {$ifend}
  PNativeInt = ^NativeInt;
  PNativeUInt = ^NativeUInt;
{$ifend}

type
  TStyle = record
    Name: string;
    FontName: string;  // default "Times New Roman"
    FontSize: Integer; // default 12
    Italic, Bold, StrikeOut: Boolean; // default False
  end;
  PStyle = ^TStyle;

  TStyleDefaults = record
    FontName: Boolean;
    FontSize: Boolean;
    Italic: Boolean;
    Bold: Boolean;
    StrikeOut: Boolean;
  end;
  PStyleDefaults = ^TStyleDefaults;

  TCell = record
    Row, Column: Integer;
    Style: string;  // default ""
    Func: string;   // default ""
    Value: Variant; // Empty, Boolean, Double, TDateTime or string. Default empty
  end;
  PCell = ^TCell;

  TCellDefaults = record
    Row: Boolean; // always default
    Column: Boolean;
    Style: Boolean;
    Func: Boolean;
    Value: Boolean;
  end;
  PCellDefaults = ^TCellDefaults;

  TRow = record
    Index: Integer;
    Cells: array of TCell;
  end;
  PRow = ^TRow;

  TSheet = record
    Name: string;
    Rows: array of TRow;
  end;
  PSheet = ^TSheet;

  TDocument = record
    Styles: array of TStyle;
    Sheets: array of TSheet;
  end;
  PDocument = ^TDocument;


function DefaultStyle(const Name: string): TStyle;

implementation

function DefaultStyle(const Name: string): TStyle;
begin
  Result.Name := Name;
  Result.FontName := 'Times New Roman';
  Result.FontSize := 12;
  Result.Italic := False;
  Result.Bold := False;
  Result.StrikeOut := False;
end;

end.
