unit Readers;

// compiler options
{$if CompilerVersion >= 24}
  {$LEGACYIFEND ON}
{$ifend}
{$if CompilerVersion >= 23}
  {$define UNITSCOPENAMES}
{$ifend}
{$U-}{$V+}{$B-}{$X+}{$T+}{$P+}{$H+}{$J-}{$Z1}{$A4}
{$ifndef VER140}
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$endif}
{$O+}{$R-}{$I-}{$Q-}{$W-}

{.$define MODECOMPACT}

interface
uses {$ifdef UNITSCOPENAMES}
       Winapi.Windows, System.SysUtils, System.Classes, System.Variants,
       System.Generics.Collections,
     {$else}
       Windows, SysUtils, Classes, Variants, Generics.Collections,
     {$endif}
     DocumentData, Generators;

// native ordinal types
{$if (not Defined(FPC)) and (CompilerVersion < 22)}
type
  {$if CompilerVersion < 21}
  NativeInt = Integer;
  NativeUInt = Cardinal;
  {$ifend}
  PNativeInt = ^NativeInt;
  PNativeUInt = ^NativeUInt;
{$ifend}

type
  TTestInformation = object
    FontCount: Cardinal;
    StringCount: Cardinal;
    DateCount: Cardinal;
    Summ: Double;

    function IsCorrect: Boolean;
    function ToString: string;
  end;
  PTestInformation = ^TTestInformation;

const
  TEST_FONT_BOLD = True;
  TEST_FONT_SIZE = 10;
  TEST_STRING_CONST = 'Pupkin';
  TEST_DATE = 42342; // EncodeDate(2015, 12, 04);

  CORRECT_TEST_INFORMATION: TTestInformation = (
  {$ifdef MODECOMPACT}
    FontCount: 49; StringCount: 58; DateCount: 25; Summ: -1897.77
  {$else}
    FontCount: 401084; StringCount: 665743; DateCount: 333788; Summ: 681160.80
  {$endif}
  );

type
  TDocumentReader = class(TObject)
  private
    FFileName: string;
    procedure DoStyleNotify(Sender: TObject; const Item: PStyle; Action: TCollectionNotification);
  protected
    FStyles: TDictionary<string, PStyle>;
    FTestInformation: TTestInformation;
  public
    class function GetDescription: string; virtual; abstract;
    class function GetDefaultFileName: string; virtual; abstract;
    class function GetGenerator: TGeneratorClass; virtual; abstract;
  public
    constructor Create(const AFileName: string); virtual;
    destructor Destroy; override;
    procedure Run; virtual; abstract;

    procedure AddStyle(const Style: TStyle);
    function FindStyle(const Name: string): PStyle;

    property FileName: string read FFileName;
    property TestInformation: TTestInformation read FTestInformation;
  end;
  TDocumentReaderClass = class of TDocumentReader;


const
  DLL_READER_PROCNAME = 'DocumentRead';

type
  TDllReaderProc = function(const FileNameA: PAnsiChar;
    const FileNameW: PWideChar): TTestInformation; cdecl;


implementation


{ TTestInformation }

function TTestInformation.IsCorrect: Boolean;
begin
  Result := (FontCount = CORRECT_TEST_INFORMATION.FontCount) and
    (StringCount = CORRECT_TEST_INFORMATION.StringCount) and
    (DateCount = CORRECT_TEST_INFORMATION.DateCount) and
    (Abs(Summ - CORRECT_TEST_INFORMATION.Summ) < 0.01);
end;

function TTestInformation.ToString: string;
begin
  Result := Format('Fonts: %d, Strings: %d, Dates: %d, Summ: %0.2f',
      [FontCount, StringCount, DateCount, Summ]);
end;


{ TDocumentReader }

constructor TDocumentReader.Create(const AFileName: string);
begin
  inherited Create;
  FFileName := AFileName;
  FStyles := TDictionary<string, PStyle>.Create();
  FStyles.OnValueNotify := DoStyleNotify;
end;

destructor TDocumentReader.Destroy;
begin
  FStyles.Free;
  inherited;
end;

procedure TDocumentReader.AddStyle(const Style: TStyle);
var
  S: PStyle;
begin
  New(S);
  S^ := Style;
  FStyles.Add(Style.Name, S);
end;

function TDocumentReader.FindStyle(const Name: string): PStyle;
begin
  if (not FStyles.TryGetValue(Name, Result)) then
    Result := nil;
end;

procedure TDocumentReader.DoStyleNotify(Sender: TObject; const Item: PStyle;
  Action: TCollectionNotification);
begin
  if (Action = cnRemoved) then
    Dispose(Item);
end;


end.
