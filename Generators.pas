unit Generators;

// compiler options
{$if CompilerVersion >= 24}
  {$LEGACYIFEND ON}
{$ifend}
{$if CompilerVersion >= 23}
  {$define UNITSCOPENAMES}
{$ifend}
{$U-}{$V+}{$B-}{$X+}{$T+}{$P+}{$H+}{$J-}{$Z1}{$A4}
{$ifndef VER140}
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$endif}
{$O+}{$R-}{$I-}{$Q-}{$W-}

{.$define MODECOMPACT}

interface
uses {$ifdef UNITSCOPENAMES}
       Winapi.Windows, System.SysUtils, System.Classes, System.Variants,
     {$else}
       Windows, SysUtils, Classes, Variants,
     {$endif}
     DocumentData;

// native ordinal types
{$if (not Defined(FPC)) and (CompilerVersion < 22)}
type
  {$if CompilerVersion < 21}
  NativeInt = Integer;
  NativeUInt = Cardinal;
  {$ifend}
  PNativeInt = ^NativeInt;
  PNativeUInt = ^NativeUInt;
{$ifend}

type
  TGenerator = class(TObject)
  private
    FFileName: string;
    function RandomStyle(const Name: string): TStyle;
    function RandomCell(const Row, Column: Integer; const Style: string): TCell;
  protected
    procedure BeginDocument(const StyleCount, SheetCount: NativeUInt); virtual;
    procedure BeginStyles; virtual;
    procedure WriteStyle(const Style: TStyle; const Defaults: TStyleDefaults); virtual;
    procedure EndStyles; virtual;
    procedure BeginSheet(const Name: string); virtual;
    procedure BeginRow(const Index: Integer; const IndexIsDefault: Boolean); virtual;
    procedure WriteCell(const Cell: TCell; const Defaults: TCellDefaults); virtual;
    procedure EndRow; virtual;
    procedure EndSheet; virtual;
    procedure EndDocument; virtual;
  public
    constructor Create(const AFileName: string); virtual;
    procedure Run;

    property FileName: string read FFileName;
  end;
  TGeneratorClass = class of TGenerator;


implementation

const
  GENERATOR_CELLS_COUNT = {10 million} 10 * 1000 * 1000;

function RandomBoolean(const Persent: Integer = 20): Boolean;
begin
  Result := (Random(100) < Persent);
end;

function RandomDouble: Double;
begin
  Result := Random(1000);

  case Random(3) of
    1: Result := Result + Random(10) / 10;
    2: Result := Result + Random(100) / 100;
  end;

  if (RandomBoolean(50)) then
    Result := -Result;
end;

function RandomDate: TDateTime;
begin
  Result := EncodeDate(2015, 12, 04) + Random(6) - 3;
end;

{ TGenerator }

constructor TGenerator.Create(const AFileName: string);
begin
  inherited Create;
  FFileName := AFileName;
end;

function TGenerator.RandomStyle(const Name: string): TStyle;
const
  FONT_NAMES: array[0..4] of string = (
    'Times New Roman',
    'Courier New',
    'Microsoft Sans Serif',
    'Arial',
    'Calibri');
  FONT_SIZES: array[0..5] of Integer = (8, 9, 11, 16, 22, 24);
begin
  Result.Name := Name;

  // font name
  Result.FontName := FONT_NAMES[Random(Length(FONT_NAMES))];

  // font size
  Result.FontSize := FONT_SIZES[Random(Length(FONT_SIZES))];

  // font styles
  Result.Italic := RandomBoolean;
  Result.Bold := RandomBoolean;
  Result.StrikeOut := RandomBoolean;
end;

function TGenerator.RandomCell(const Row, Column: Integer;
  const Style: string): TCell;
const
  FUNC_VALUES: array[0..3] of string = (
    'SUM(params)',
    'CONVERT(params)',
    'BITXOR(params)',
    'HEX2DEC(params)'
  );
  STR_VALUES: array[0..5] of string = (
    'London is the capital of Great Britain',
    'The Embarcadero is the eastern waterfront and roadway of the Port of San Francisco, San Francisco, California',
    'Vasily Pupkin walks along the promenade',
    'We know a wonderful person - Vasily Pupkin',
    'We know another Vasily - Pupkoff',
    'Seattle is the largest city in both the state of Washington and the Pacific Northwest region of North America'
  );
begin
  // row, column, style
  Result.Row := Row;
  Result.Column := Column;
  Result.Style := Style;

  // func
  if (RandomBoolean) then
  begin
    Result.Func := FUNC_VALUES[Random(Length(FUNC_VALUES))];
  end else
  begin
    Result.Func := '';
  end;

  // value
  if (RandomBoolean) then
  begin
    Result.Value := UnAssigned;
  end else
  begin
    //Boolean, Double, TDateTime or string
    case Random(4) of
      0: Result.Value := RandomBoolean(50);
      1: Result.Value := RandomDouble;
      2: Result.Value := RandomDate;
    else
      // 3
      Result.Value := STR_VALUES[Random(Length(STR_VALUES))];
    end;
  end;
end;

procedure TGenerator.Run;
var
  i: NativeUInt;
  StyleCount: NativeUInt;
  SheetCount: NativeUInt;

  MarginCells: NativeUInt;
  SheetCells: NativeUInt;

  IsDefault: Boolean;
  StyleName: string;
  RowIndex, ColumnIndex, j: NativeUInt;
  CellCount: NativeUInt;

  Style: TStyle;
  StyleDefaults: TStyleDefaults;
  Cell: TCell;
  CellDefaults: TCellDefaults;
begin
  System.RandSeed := 0; // Base for random number generator

  StyleCount := 10 + Random(10);
  SheetCount := 2 + Random(5);
  MarginCells := GENERATOR_CELLS_COUNT {$ifdef MODECOMPACT}div 10000{$endif};

  BeginDocument(StyleCount, SheetCount);
  begin
    // styles
    BeginStyles;
    for i := 1 to StyleCount do
    begin
      Style := RandomStyle('Style' + IntToStr(i));
      StyleDefaults.FontName := (Style.FontName = 'Times New Roman');
      StyleDefaults.FontSize := (Style.FontSize = 12);
      StyleDefaults.Italic := not Style.Italic;
      StyleDefaults.Bold := not Style.Bold;
      StyleDefaults.StrikeOut := not Style.StrikeOut;

      WriteStyle(Style, StyleDefaults);
    end;
    EndStyles;

    // sheets
    for i := 1 to SheetCount do
    begin
      BeginSheet('Sheet' + IntToStr(i));

      // sheet cells
      if (i = SheetCount) then
      begin
        SheetCells := MarginCells;
      end else
      begin
        SheetCells := (MarginCells div (SheetCount - i + 1))
          {$ifNdef MODECOMPACT}+ NativeUInt(Random(10000) - 5000){$endif};
      end;
      Dec(MarginCells, SheetCells);

      // rows
      RowIndex := 0;
      while (SheetCells <> 0) do
      begin
        IsDefault := RandomBoolean(80);
        RowIndex := RowIndex + 1 + NativeUInt(not IsDefault);

        BeginRow(RowIndex, IsDefault);
        begin
          CellCount := Random(100);
          if (CellCount > SheetCells) then CellCount := SheetCells;
          Dec(SheetCells, CellCount);

          // cells
          ColumnIndex := 0;
          for j := 1 to CellCount do
          begin
            IsDefault := RandomBoolean(80);
            ColumnIndex := ColumnIndex + 1 + NativeUInt(not IsDefault);

            StyleName := '';
            if (RandomBoolean) then
              StyleName := 'Style' + IntToStr(1 + Random(StyleCount));

            Cell := RandomCell(RowIndex, ColumnIndex, StyleName);
            CellDefaults.Row := True; // always default
            CellDefaults.Column := IsDefault;
            CellDefaults.Style := (Cell.Style = '');
            CellDefaults.Func := (Cell.Func = '');
            CellDefaults.Value := VarIsEmpty(Cell.Value);
            WriteCell(Cell, CellDefaults);
          end;
        end;
        EndRow;
      end;

      EndSheet;
    end;
  end;
  EndDocument;
end;

procedure TGenerator.BeginDocument(const StyleCount, SheetCount: NativeUInt);
begin
end;

procedure TGenerator.EndDocument;
begin
end;

procedure TGenerator.BeginStyles;
begin
end;

procedure TGenerator.EndStyles;
begin
end;

procedure TGenerator.WriteStyle(const Style: TStyle; const Defaults: TStyleDefaults);
begin
end;

procedure TGenerator.BeginSheet(const Name: string);
begin
end;

procedure TGenerator.EndSheet;
begin
end;

procedure TGenerator.BeginRow(const Index: Integer;
  const IndexIsDefault: Boolean);
begin
end;

procedure TGenerator.EndRow;
begin
end;

procedure TGenerator.WriteCell(const Cell: TCell; const Defaults: TCellDefaults);
begin
end;



end.
