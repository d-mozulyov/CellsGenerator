
                      VERSION HISTORY

Under the name "SAXExpat"

0.9 (beta 1) - October 25, 2001
0.9 (beta 2) - November 4, 2001
0.9 (beta 3) - November 6, 2001
0.9 (beta 4) - November 11, 2001
0.9 (beta 5) - November 30, 2001
0.9 (beta 6) - March 8, 2002
0.9 (beta 7) - March 15, 2002
0.9 (beta 8) - March 17, 2002
0.9 (beta 9) - April 27, 2002
1.0          - June 6, 2002
1.1          - July 19, 2002
1.2          - February 16, 2003
1.21         - February 20, 2003
1.3          - July 15, 2003
1.31         - July 18, 2003
1.32         - July 23, 2003
1.33         - July 25, 2003
1.34         - July 26, 2003

Under the name "KDS XML"

1.4          - February 12, 2004
1.41         - April 7, 2004

