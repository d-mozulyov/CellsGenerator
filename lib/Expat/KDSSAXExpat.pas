(*
This software is licensed according to the "Modified BSD License",
where the following substitutions are made in the license template:
  <OWNER> = Karl Waclawek
  <ORGANIZATION> = Karl Waclawek
  <YEAR> = 2001
It can be obtained from http://opensource.org/licenses/bsd-license.html.
*)

{
                  SAX 2 Wrapper for Expat Parser
}

unit KDSSAXExpat;

{$I Expat.inc}

// remember if we have overflow checking turned on
{$IFOPT Q+}
  {$DEFINE _KDSExpat_Q+}
{$ENDIF}

interface

{                                 Notes

* Constraint:
  Works only with Expat 1.95.6 and later - see included files.
  To work with a later version than 1.95.6, edit ExpatVersion.inc
  and define one of these symbols: EXPAT_1_95_7, or EXPAT_1_95_8, etc.

* Memory Management:
  There is an option to have the Expat parser use Delphi's memory allocation
  routines - the MMSuite constant can be passed to XML_ParserCreateMM().
  Note: in case the application's memory manager changes at runtime,
    the global procedure UpdateMemoryManager must be called, so that
    the Expat parser will use the new memory manager.

* If the expat library was built with XML_UNICODE and/or XML_UNICODE_WCHAR_T
  defined, it will pass XML data encoded as UTF-16 (WideChars), otherwise
  as UTF-8 (Chars);
  in case of UTF16, define SAX_WIDESTRINGS (this should implicitly also
  define XML_UNICODE needed for the Expat.pas interface unit), otherwise
  the library will not compile.

* Expat specific functionality:
  - The XMLReader can be "QueryInterfaced" for the IExpatXMLReader interface,
    which exposes some Expat specific functionality, and also gives access to
    the currently active Expat parser instance, so that the functions declared
    in Expat.pas can be called on it directly.
  - The locator passed to SetDocumentLocator can be hard cast to IExpatLocator,
    which gives access to the parse position in the current input stream;
    since the current input stream changes when an external entity is handled,
    one cannot access the parent documents parse position (or column/line number),
    unless one saves it in the StartEntity callback.

* Direct access to Features/Properties
  The XMLReader can be "QueryInterfaced" for the IBufferedXMLReaderEx
  interface, which exposes the Features and Properties directly, for more
  efficient access.

* IContentHandler.IgnorableWhiteSpace(PSAXChar(S), Len) will not be
  called because the parser is non-validating; such white space will
  be reported as character data, since the parser cannot determine
  whether the element is supposed to contain character data.

* Note - This is how entities are reported:
  //  ExternalGeneral  ... external-general-entities feature
  //  LexicalParameter ... lexical-handler/parameter-entities feature
  //  SkipInternal     ... skip-internal-entities feature
  External General Entities:
  - ExternalGeneral = True: ILexicalHandler.Start/EndEntity is called
  - ExternalGeneral = False: IContentHandler.SkippedEntity is called
  External Parameter Entities - LexicalParameter = True:
  - ExternalParameter = True: ILexicalHandler.Start/EndEntity is called
  - ExternalParameter = False: IContentHandler.SkippedEntity is called
  External Parameter Entities - LexicalParameter = False:
  - No calls, external parameter entities are silently ignored
  Internal Entities - LexicalParameter = True:
  - SkipInternal = True: IContentHandler.SkippedEntity is called
  - SkipInternal = False: No calls, silently expanded
  Internal Entities - LexicalParameter = False:
  - Parameter entities are not parsed, i.e. silently ignored

  Internally, LexicalParameterFeature translates to the ParameterEntities
  feature, which turns parameter entity parsing on or off; that is, not
  even SkippedEntity will be called (also depending on the value of the
  ParseUnlessStandalone property); this is also how MSXML4 implements it,
  but it is not certain that this conforms to SAX2.

                         Supported Features

  (standard features - prefixed with "http://xml.org/sax/features/")

  namespaces:                    read/write, default True
  namespace-prefixes:            read/write, fixed False (QNames still reported)
  validation:                    read/write, fixed False
  external-general-entities:     read/write, default False
  external-parameter-entities:   read/write, default False
  is-standalone:                 read-only,  valid while parsing
  use-attributes2:               read-only,  depends on CreateAttributes override
  use-locator2:                  read-only,  fixed True
  use-entity-resolver2:          read-only,  fixed True
  resolve-dtd-uris:              read-only,  fixed False
  lexical-handler/parameter-entities: read/write, default True (with limitations)

  (extension features - prefixed with "http://kd-soft.net/sax/features/")

  parameter-entities:            read/write, default True
    turns parameter entity parsing on or off, including the external subset,
    the internal subset (except for PEs) is still being parsed
  skip-internal-entities:        read/write, default False
    skip or (silently) expand internal entities (parameter and general)
  parse-unless-standalone:       read/write, default False
    when True then external parameter entities will not be parsed
    if the XML declaration specifies standalone = "yes"
  standalone-error:              read/write, default False
    determines if parser returns an error, when it encounters an external
    subset or a reference to a parameter entity, but does have standalone
    set to "yes" in the XML declaration

  (extension features - prefixed with "http://saxforpascal.org/features/")

  use-reader-control:            read-only, depends on compile options
    returns if the parser implements the IXMLReaderControl  interface;
    should be true if EXPAT_1_95_8 or later is defined

  Notes:
  - "read/write, fixed False" means that the feature can be set, but
    only to the fixed value False, otherwise an exception will be raised
  - The namespaces feature should be set *before* parsing is started.
    If set later, it will only affect the reporting of QNames, that is,
    if namespaces was True at the beginning, then QName/Prefix reporting can
    be turned of during parsing, but namespace processing will stay active.

                         Supported Properties

  (names considered prefixed with "http://xml.org/sax/properties/")

  declaration-handler:           read/write
  lexical-handler:               read/write

  (names considered prefixed with "http://kd-soft.net/sax/properties/")

  default-handler:               read/write
    get/set default handler - reports events for which no handler is set

                       Limitations/Deviations

* No Validation:
  The parser processes DTD declarations, but does not validate against them.

* LexicalHandler, "parameter-entities" feature:
  ILexicalHandler.Start/EndEntity are only called for external entities,
  internal entities are not reported (except when skipped).

* Enumerated attributes:
  Enumerated attributes are reported as type 'ENUMERATION' instead of
  'NMTOKEN' - see Open Issues below.

* StartDocument()/EndDocument() call-backs:
  The rules for calling these are not clear in the SAX specs.
  This implementation works as follows:
  - Once StartDocument() was called, EndDocument() will be called as well,
    even after a fatal error, but not when an exception was raised or when
    parsing was aborted by calling IXMLReaderControl.Abort().
  - If StartDocument() was not called, EndDocument will not be called either.
  - This is independent of whether a content handler was assigned or not, so
    it should maybe read: "Once StartDocument() would have been called, ...".

* EntityResolver2:
  When IEntityResolver2.GetExternalSubset is called for a document without
  a DOCTYPE declaration, then the Name argument will be empty since
  Expat does not read ahead to determine the name of the root element;
  this should not be a problem, however, because the application which
  provides the external subset, should use information from the application
  context to supply the correct DTD, and not rely on the Name argument.

* Input sources:
  Parser can currently only use IStreamInputSource and IReaderInputSource.

* BaseURI, "resolve-dtd-uris" feature (can be false only):
  Since SAX2 does not accept BaseURIs as arguments for IDTDHandler.NotationDecl(),
  IDTDHandler.UnparsedEntityDecl() and IDeclHandler.ExternalEntityDecl(), the
  parser would have to resolve relative SystemId URIs for these handlers itself
  if this feature were true. However, the parser cannot do that currently .

* Feature "http://xml.org/sax/features/namespace-prefixes":
  The underlying Expat parser never reports xmlns attributes when namespace
  processing is turned on, so this feature cannot be set to True.
  Nevertheless, prefixes are still reported (in QNames) when the namespaces
  feature is turned on.

* IXMLReaderControl:
  The Suspend() function will not work while an external parameter entity
  is being parsed. It will silently fail, that is, one should check
  the IXMLReaderControl.Status property immediately afterwards.
  Other events where Suspend() will fail silently, are EndDocument()
  and EndEntity(), even when correctly called while Status = xrsParsing.
  
                            Open Issues

* For reporting internal entities:
  Check out XML_GetCurrentByteCount - returns 0 if current event called
  from inside an internal entity reference - maybe that can be used.

* It is not certain that the way LexicalParameterFeature is implemented
  conforms to SAX2. It seems that if LexicalParameterFeature = False,
  parameter entities should still be reported if skipped, but this
  is not how it is done in this implementation.

* We probably need some kind of URI parser/resolver that can use
  the BaseURI passed to various Expat handlers to build absolute
  URIs from the SystemIds (or PublicIds)  to be passed to the
  SAX2 handlers - see above (under Notes).

* Where does the SAXLocator get the SystemId and PublicId? 
  - They are defined like this:
    "The return value is the public/system identifier of the document
     entity or of the external parsed entity in which the markup triggering
     the event appears"
  - Currently we store them for the current parse level, populated from
    the input source or in calls to ExternalEntityRefHandler
  
* Attributes.GetType: what about enumerated types to be reported as 'NMTOKEN'?
  This is implemented to follow the XML-Infoset requirements (check
  http://www.w3.org/TR/xml-infoset/ rather than the SAX specs.
  So, SAXExpat reports 'ENUMERATION' instead of 'NMTOKEN'.

}

uses
  SysUtils, Classes, SAX, SAXExt, BSAX, BSAXExt, SFP, KDSClasses, KDSSAX,
  KDSXML, Expat, KDSXMLHelpers, KDSSAXHelpers, KDSHashTable, KDSDblHashTable;

resourcestring
  // SNameLengthExceeded = 'Maximum name length exceeded';
  // SExpatParserFailure = 'Failure loading Expat parser';
  SCannotGetInputContext = 'This Expat parser does not return a parse position';
  SUnexpectedParserResult = 'Unexpected parser result';
  SInitParseError = 'Failed to initialize parsing';
  SStreamReaderSourceOnly = 'Can only process IStream- and IReaderInputSource';

const
  XML_SAXVENDOR = 'SAXExpat';
  XML_NSSEP = SAXChar(#$1F);       // ASCII US (unit separator)
  XML_CONTEXTSEP = SAXChar(#$0C);  // ASCII DC2 (device control 2)

  XML_READBUFSIZE = 16384;
  // XML_MAX_NAME_LEN = 1024;

type
  ESAXExpat = class(ESAXException);

  TExpatLocator = class;

  // ESAXExpatParse does not own FLocator!
  ESAXExpatParse = class(ESAXExpat)
    private
      FLocator: TExpatLocator;
      FErrorCode: Integer;
    protected
      procedure SetLocator(Value: TExpatLocator);
      procedure Init(const Msg: string; Locator: TExpatLocator;
        ErrorCode: Integer);
    public
      constructor Create(const Msg: string; Locator: TExpatLocator;
        ErrorCode: Integer); overload;
      destructor Destroy; override;
      property ErrorCode: Integer read FErrorCode;
      property Locator: TExpatLocator read FLocator;
    end;

  { SAX2 Extensions - direct access instead of  using Features/Properties }
  IBufferedXMLReaderEx = interface
    ['{3B0965CC-955B-4B04-B707-D85B283422DC}']
    function GetLexicalHandler: IBufferedLexicalHandler;
    procedure SetLexicalHandler(const Handler: IBufferedLexicalHandler);
    function GetDeclHandler: IBufferedDeclHandler;
    procedure SetDeclHandler(const Handler: IBufferedDeclHandler);
    function GetNamespaces: Boolean;
    procedure SetNamespaces(Value: Boolean);
    function GetNamespacePrefixes: Boolean;
    procedure SetNamespacePrefixes(Value: Boolean);
    function GetValidation: Boolean;
    procedure SetValidation(Value: Boolean);
    function GetExternalGeneral: Boolean;
    procedure SetExternalGeneral(Value: Boolean);
    function GetExternalParameter: Boolean;
    procedure SetExternalParameter(Value: Boolean);
    function IsStandalone: Boolean;
    function GetUseAttributes2: Boolean;
    function GetUseLocator2: Boolean;
    function GetUseEntityResolver2: Boolean;
    function GetResolveDTDURIs: Boolean;
    property LexicalHandler: IBufferedLexicalHandler
      read GetLexicalHandler write SetLexicalHandler;
    property DeclHandler: IBufferedDeclHandler
      read GetDeclHandler write SetDeclHandler;
    property Namespaces: Boolean
      read GetNamespaces write SetNamespaces;
    property NamespacePrefixes: Boolean
      read GetNamespacePrefixes write SetNamespacePrefixes;
    property Validation: Boolean
      read GetValidation write SetValidation;
    property ExternalGeneral: Boolean
      read GetExternalGeneral write SetExternalGeneral;
    property ExternalParameter: Boolean
      read GetExternalParameter write SetExternalParameter;
    property UseAttributes2: Boolean
      read GetUseAttributes2;
    property UseLocator2: Boolean
      read GetUseLocator2;
    property UseEntityResolver2: Boolean
      read GetUseEntityResolver2;
    property ResolveDTDURIs: Boolean
      read GetResolveDTDURIs;
    end;

  { Expat extension to locator interfaces }
  IExpatLocator = interface(ILocator2)
    ['{68191C17-31E6-490A-A6EE-2E89E9C0BC2F}']
    function GetParsePosition: Integer; // parse position in current input stream
    property ParsePosition: Integer
      read GetParsePosition;
    end;

  { Expat Extensions }
  IExpatXMLReader = interface
    ['{EEBED69A-ECF2-4BF7-A4B1-22155667614A}']
    function GetParser: TXMLParser;     // currently active Expat parser instance
    function GetDefaultDataHandler: IBufferedDefaultHandler;
    procedure SetDefaultDataHandler(const Handler: IBufferedDefaultHandler);
    function GetBaseURI: PSAXChar;
    procedure SetBaseURI(Value: PSAXChar);
    function GetParameterEntities: Boolean;
    procedure SetParameterEntities(Value: Boolean);
    function GetSkipInternal: Boolean;
    procedure SetSkipInternal(Value: Boolean);
    function GetParseUnlessStandalone: Boolean;
    procedure SetParseUnlessStandalone(Value: Boolean);
    function GetStandaloneError: Boolean;
    procedure SetStandaloneError(Value: Boolean);
    function GetHasEntityResolver2: Boolean;
    function GetExpatVersion: SAXString;
    function GetForeignDoctypeName: SAXString;
    procedure SetForeignDoctypeName(const Value: SAXString);
    // content model as returned by Expat - *only* valid in ElementDecl handler!
    function GetContentModel: PXMLContent;
    property Parser: TXMLParser read GetParser;
    property DefaultDataHandler: IBufferedDefaultHandler
      read GetDefaultDataHandler write SetDefaultDataHandler;
    property BaseURI: PSAXChar
      read GetBaseURI write SetBaseURI;
    property ParameterEntities: Boolean
      read GetParameterEntities write SetParameterEntities;
    property SkipInternal: Boolean
      read GetSkipInternal write SetSkipInternal;
    property ParseUnlessStandalone: Boolean
      read GetParseUnlessStandalone write SetParseUnlessStandalone;
    property StandaloneError: Boolean
      read GetStandaloneError write SetStandaloneError;
    property HasEntityResolver2: Boolean
      read GetHasEntityResolver2;
    property ExpatVersion: SAXString
      read GetExpatVersion;
    property ForeignDoctypeName: SAXString
      read GetForeignDoctypeName write SetForeignDoctypeName;
    property ContentModel: PXMLContent read GetContentModel;
    end;

  // this interface should be used for errors *not* returned by Expat itself
  IExpatParseError = interface
    ['{B5E45730-5800-4DB3-BB78-F7BDEF2561FD}']
    function GetError: Exception;
    end;

  // this interface should be used for errors returned by Expat itself
  IExpatNativeParseError = interface(IExpatParseError)
    ['{2AA84334-8E34-445D-9C39-253AFC2EB8CE}']
    function GetErrorCode: Integer; // Expat error code; 0 if no error
    end;

  TExpatXMLReader = class;

  TExpatAttributes = class(TContainedObject, IUnknown, IBufferedAttributes,
    IBufferedAttributes2)
    private
      FAtts: PAttrs;
      FElement: PSAXChar;   // Qualified/literal name of current element
      FElementLen: Integer;
      FAttrDecls: TSAXAttributeDecls;
      FTotalCount: Integer;
      FSpecCount: Integer;
      FQNameBuf: PSAXChar;  // reusable buffer for QName
      FQNameBufLen: Integer;
    protected
      property SpecCount: Integer read FSpecCount;
      procedure CheckIndex(Index: Integer);
      procedure NotFoundError(QName: PSAXChar; QNameLen: Integer); overload;
      procedure NotFoundError(Uri: PSAXChar; UriLen: Integer;
        LocalName: PSAXChar; LocalNameLen: Integer); overload;
      // under MSXML, GetName is part of ISAXAttributes
      procedure GetName(Index: Integer; out Uri: PSAXChar; out UriLen: Integer;
        out LocalName: PSAXChar; out LocalNameLen: Integer; out QName: PSAXChar;
        out QNameLen: Integer);
      { IBufferedAttributes }
      function GetLength: Integer;
      procedure GetURI(Index: Integer; out Uri: PSAXChar; out UriLen: Integer);
      procedure GetLocalName(Index: Integer; out LocalName: PSAXChar;
        out LocalNameLen: Integer);
      procedure GetQName(Index: Integer; out QName: PSAXChar;
        out QNameLen: Integer);
      procedure GetType(Index: Integer; out AType: PSAXChar;
        out ATypeLen: Integer); overload;
      procedure GetType(Uri: PSAXChar; UriLen: Integer; LocalName: PSAXChar;
        LocalNameLen: Integer; out AType: PSAXChar; out ATypeLen: Integer); overload;
      procedure GetType(QName: PSAXChar; QNameLen: Integer; out AType: PSAXChar;
        out ATypeLen: Integer); overload;
      procedure GetValue(Index: Integer; out Value: PSAXChar;
        out ValueLen: Integer); overload;
      procedure GetValue(Uri: PSAXChar; UriLen: Integer; LocalName: PSAXChar;
        LocalNameLen: Integer; out Value: PSAXChar; out ValueLen: Integer);
        overload;
      procedure GetValue(QName: PSAXChar; QNameLen: Integer;
        out Value: PSAXChar; out ValueLen: Integer); overload;
      function GetIndex(Uri: PSAXChar; UriLen: Integer; LocalName: PSAXChar;
        LocalNameLen: Integer): Integer; overload;
      function GetIndex(QName: PSAXChar; QNameLen: Integer): Integer; overload;
      { IBufferedAttributes2 }
      function IsDeclared(Index: Integer): Boolean; overload;
      function IsDeclared(Uri: PSAXChar; UriLen: Integer; LocalName: PSAXChar;
        LocalNameLen: Integer): Boolean; overload;
      function IsDeclared(QName: PSAXChar; QNameLen: Integer): Boolean; overload;
      function IsSpecified(Index: Integer): Boolean; overload;
      function IsSpecified(Uri: PSAXChar; UriLen: Integer; LocalName: PSAXChar;
        LocalNameLen: Integer): Boolean; overload;
      function IsSpecified(QName: PSAXChar; QNameLen: Integer): Boolean; overload;
    public
      constructor Create(Reader: TExpatXMLReader);
      destructor Destroy; override;
      procedure Initialize(ElName: PSAXChar; ElNameLen: Integer;
        const Atts: TAttrs; SpecAttrCount: Integer);
      property AttrDecls: TSAXAttributeDecls read FAttrDecls;
    end;

  TExpatParEntDeclHandler = class(THashItemHandler)
    protected
      procedure FreeParEntDecl(ItemP: Pointer);
    public
      constructor Create;
      class function GetKey(ItemP: Pointer): Pointer; override;
      class function CompareKey(HashKeyP1, HashKeyP2: Pointer): Boolean; override;
      function Hash(HashKeyP: Pointer): THashValue; override;
    end;

  { keeps track of external parameter entities, since Expat does not
    provide their names in the ExternalEntityRefHandler callback     }
  TExpatParEntDecls = class
    private
      FEntityHandler: TExpatParEntDeclHandler;
      FEntityTable: TDblHashTable;
      FHashCursor: TDblHashTableCursor;
    public
      constructor Create;
      destructor Destroy; override;
      function AddDecl(const Name, PublicId, SystemId: SAXString): Boolean;
      function FindDecl(PublicId, SystemId: PSAXChar): Boolean;
      procedure Clear;
      function DeclExists: Boolean;
      procedure GetDecl(out Name: SAXString);
      property HashCursor: TDblHashTableCursor read FHashCursor;
    end;

  TExpatLocator = class(TInterfacedObject, ILocator, ILocator2, IExpatLocator)
    private
      FReader: TExpatXMLReader;
    protected
      { ILocator }
      function GetColumnNumber: Integer;
      function GetLineNumber: Integer;
      function GetPublicId: PSAXChar;
      function GetSystemId: PSAXChar;
      { ILocator2 }
      function GetEncoding: PSAXChar;
      function GetXMLVersion: PSAXChar;
      { IExpatLocator }
      function GetParsePosition: Integer;
    public
      constructor Create(Reader: TExpatXMLReader);
    end;

  TExpatFeatures = class
    public
      class function GetFeature(Reader: TExpatXMLReader;
        const Name: SAXString): Boolean; virtual; abstract;
      class procedure SetFeature(Reader: TExpatXMLReader;
        const Name: SAXString; Value: Boolean); virtual; abstract;
    end;
  TExpatFeaturesClass = class of TExpatFeatures;

  TExpatProperties = class(TContainedObject)
    private
      FReader: TExpatXMLReader;
    public
      constructor Create(Reader: TExpatXMLReader);
    end;

  TExpatParseError = class;
  TExpatNativeParseError = class;

  TDocParseData = record
    Locator: TExpatLocator;
    ParseException: ESAXExpatParse;
    Started: Boolean;
    Standalone: Integer;
    NameBuf: PSAXChar;
    NameBufLen: Integer;
    ContentStream: TXMLStringStream;
    HasDocTypeDecl: Boolean;
    ForeignDTDSource: IInputSource;
    // access to content model passed in ElementDeclHandler
    ContentModel: PXMLContent;
    end;

  TEntityParseData = class
    private
      FSource: IInputSource; // to keep FInStream alive
      FParser: TXMLParser;
      // only one of these two can be non-null
      FInStream: TStream;  // extracted from input source
      FInReader: IReader;  // extracted from input source
      FName: PSAXChar;
      FNameLen: Integer;
      FEncoding: SAXString;
      FVersion: SAXString;
      FPublicId: SAXString;
      FSystemId: SAXString;
      FParent: TEntityParseData;
    public
      procedure Init(const Source: IInputSource; IsStreamSource: Boolean;
        Parser: TXMLParser; Name: PSAXChar = nil; NameLen: Integer = 0);
      procedure Clear;
      property Parser: TXMLParser read FParser;
      // only one of these two can be non-null
      property InStream: TStream read FInStream;  // extracted from Source
      property InReader: IReader read FInReader;  // extracted from Source
      property Name: PSAXChar read FName;
      property NameLen: Integer read FNameLen;
      property Version: SAXString read FVersion;
      property Encoding: SAXString read FEncoding;
      property PublicId: SAXString read FPublicId;
      property SystemId: SAXString read FSystemId;
      property Parent: TEntityParseData read FParent;
    end;

  TDocParseStatus = (dpsFinished, dpsSuspended, dpsFatalError, dpsAborted);

  TExpatXMLReader = class(TInterfacedObject, IBaseXMLReader,
    IBufferedXMLReader, IBufferedXMLReaderEx, IExpatXMLReader
{$IFDEF EXPAT_1_95_8_UP},
    IXMLReaderControl
{$ENDIF}
    )
    private
      FDefaultDataHandler: IBufferedDefaultHandler;
      FEntityResolver: IEntityResolver;
      FHasEntityResolver2: Boolean;
      FContentHandler: IBufferedContentHandler;
      FDTDHandler: IBufferedDTDHandler;
      FErrorHandler: IErrorHandler;
      FLexicalHandler: IBufferedLexicalHandler;
      FDeclHandler: IBufferedDeclHandler;
      FAttributes: TExpatAttributes;
      FParamEntityDecls: TExpatParEntDecls;
      FProperties: TExpatProperties;
      FNamespaces: Boolean;
      FNamespacePrefixes: Boolean; 
      FValidation: Boolean;
      FExternalGeneral: Boolean;
      FExternalParameter: Boolean;
      FParameterEntities: Boolean;
      FStandaloneError: Boolean;
      FParseUnlessStandalone: Boolean;
      FSkipInternal: Boolean;
      FForeignDoctypeName: SAXString;
      FParseError: TExpatParseError;
      FNativeParseError: TExpatNativeParseError;
      // holds data used while parsing
      FParseData: TDocParseData;
      // stack of TEntityParseData instances, used while parsing, entity specific
      FEntityParseData: TEntityParseData;
      // linked list of unused TEntityParseData instances
      FFreeEntityParseData: TEntityParseData;
    protected
      function CreateAttributes: TExpatAttributes; virtual;
      class function ExpatFeatures: TExpatFeaturesClass; virtual;
      function CreateProperties: TExpatProperties; virtual;
      procedure ConfigureExpatHandlersForParsing; virtual;
      procedure ConfigureExpatForParsing; virtual;
      function ExpatGetParamEntityParsing: Boolean;
      procedure DisposeEntityParsedata(EPDList: TEntityParseData);
      procedure PushEntityParseData;
      procedure PushDocumentEntityParseData(const Source: IInputSource;
        IsStreamSource: Boolean);
      function PushChildEntityParseData(ParentParser: TXMLParser;
        Name: PSAXChar; NameLen: Integer; Context: PSAXChar;
        const Source: IInputSource; IsStreamSource: Boolean): Boolean;
      procedure PopEntityParseData;
      procedure CleanupParseData;
      function ResolveExternalEntity(Name: PSAXChar; NameLen: Integer;
        PubId, BaseURI, SysId: PSAXChar): IInputSource;
      function ResolveForeignDTD(Name: PSAXChar; NameLen: Integer): IInputSource;
      function ContinueEntityParsing: TDocParseStatus;
{$IFDEF EXPAT_1_95_8_UP}
      function ResumeParsing: TDocParseStatus;
{$ENDIF}
      procedure FinishExternalEntityParsing;
      function StartParsing(const Source: IInputSource;
        IsStreamSource: Boolean): TDocParseStatus;
      procedure FinishParsing;
      procedure ParseSource(const Source: IInputSource; IsStreamSource: Boolean);
      procedure CallErrorHandler(ErrType: TSAXErrorType; const Msg: SAXString); overload;
      procedure CallErrorHandler(ErrType: TSAXErrorType; var Error: Exception); overload;
      procedure CallNativeErrorHandler(ErrType: TSAXErrorType; var Error: ESAXExpatParse);
      function ProcessExpatError(Parser: TXMLParser): TSAXErrorType;
      property Attributes: TExpatAttributes read FAttributes;
      property ParamEntityDecls: TExpatParEntDecls read FParamEntityDecls;
      function GetStandalone: TXMLDocStandalone;
      function GetReaderControl: Boolean;
      function Parsing: Boolean;
      { IBaseXMLReader }
      function GetFeature(const Name: SAXString): Boolean;
      procedure SetFeature(const Name: SAXString; Value: Boolean);
      function GetProperty(const Name: SAXString): IProperty;
      function GetEntityResolver: IEntityResolver;
      procedure SetEntityResolver(const Resolver: IEntityResolver);
      function GetErrorHandler: IErrorHandler;
      procedure SetErrorHandler(const Handler: IErrorHandler);
      procedure Parse(const Input: IInputSource); overload;
      procedure Parse(const SystemId: SAXString); overload;
      { IBufferedXMLReader }
      function GetContentHandler: IBufferedContentHandler;
      procedure SetContentHandler(const Handler: IBufferedContentHandler);
      function GetDTDHandler: IBufferedDTDHandler;
      procedure SetDTDHandler(const Handler: IBufferedDTDHandler);
{$IFDEF EXPAT_1_95_8_UP}
      { IXMLReaderControl }
      // will not work in external parameter entities
      procedure Suspend;
      procedure Abort;
      procedure Resume;
      function GetStatus: TXMLReaderStatus;
{$ENDIF}
      { IBufferedXMLReader related }
      procedure SetContentHandlerForParsing;
      procedure SetDTDHandlerForParsing;
      { IBufferedXMLReaderEx - SAX2 extensions }
      function GetLexicalHandler: IBufferedLexicalHandler;
      procedure SetLexicalHandler(const Handler: IBufferedLexicalHandler);
      function GetDeclHandler: IBufferedDeclHandler;
      procedure SetDeclHandler(const Handler: IBufferedDeclHandler);
      function GetNamespaces: Boolean;
      procedure SetNamespaces(Value: Boolean);
      procedure SetNameSpacesForParsing;
      function GetNamespacePrefixes: Boolean;
      procedure SetNamespacePrefixes(Value: Boolean);
      function GetValidation: Boolean;
      procedure SetValidation(Value: Boolean);
      function GetExternalGeneral: Boolean;
      procedure SetExternalGeneral(Value: Boolean);
      function GetExternalParameter: Boolean;
      procedure SetExternalParameter(Value: Boolean);
      function IsStandalone: Boolean;
      function GetUseAttributes2: Boolean; virtual;
      function GetUseLocator2: Boolean; virtual;
      function GetUseEntityResolver2: Boolean;  virtual;
      function GetResolveDTDURIs: Boolean;
      { IBufferedXMLReaderEx related }
      procedure SetLexicalHandlerForParsing;
      procedure SetDeclHandlerForParsing;
      { IExpatXMLReader }
      function GetParser: TXMLParser;   // currently active Expat parser instance
      function GetDefaultDataHandler: IBufferedDefaultHandler;
      procedure SetDefaultDataHandler(const Handler: IBufferedDefaultHandler);
      function GetBaseURI: PSAXChar;
      procedure SetBaseURI(Value: PSAXChar);
      function GetParameterEntities: Boolean;
      procedure SetParameterEntities(Value: Boolean);
      function GetSkipInternal: Boolean;
      procedure SetSkipInternal(Value: Boolean);
      function GetParseUnlessStandalone: Boolean;
      procedure SetParseUnlessStandalone(Value: Boolean);
      function GetStandaloneError: Boolean;
      procedure SetStandaloneError(Value: Boolean);
      function GetHasEntityResolver2: Boolean;
      function GetExpatVersion: SAXString;
      function GetForeignDoctypeName: SAXString;
      procedure SetForeignDoctypeName(const Value: SAXString);
      function GetContentModel: PXMLContent;
      { IExpatXMLReader related }
      procedure SetDefaultDataHandlerForParsing;
      procedure SetParameterEntitiesForParsing;
      { SAX2 Extension Properties - for internal use }
      property LexicalHandler: IBufferedLexicalHandler
        read GetLexicalHandler write SetLexicalHandler;
      property DeclHandler: IBufferedDeclHandler
        read GetDeclHandler write SetDeclHandler;
      property Namespaces: Boolean
        read GetNamespaces write SetNamespaces;
      property NamespacePrefixes: Boolean
        read GetNamespacePrefixes write SetNamespacePrefixes;
      property Validation: Boolean
        read GetValidation write SetValidation;
      property ExternalGeneral: Boolean
        read GetExternalGeneral write SetExternalGeneral;
      property ExternalParameter: Boolean
        read GetExternalParameter write SetExternalParameter;
      property UseAttributes2: Boolean
        read GetUseAttributes2;
      property UseLocator2: Boolean
        read GetUseLocator2;
      property UseEntityResolver2: Boolean
        read GetUseEntityResolver2;
      { Expat Extension Properties - for internal use }
      property DefaultDataHandler: IBufferedDefaultHandler
        read GetDefaultDataHandler write SetDefaultDataHandler;
      property BaseURI: PSAXChar
        read GetBaseURI write SetBaseURI;
      property ParameterEntities: Boolean
        read GetParameterEntities write SetParameterEntities;
      property SkipInternal: Boolean
        read GetSkipInternal write SetSkipInternal;
      property ParseUnlessStandalone: Boolean
        read GetParseUnlessStandalone write SetParseUnlessStandalone;
      property StandaloneError: Boolean
        read GetStandaloneError write SetStandaloneError;
      property HasEntityResolver2: Boolean
        read GetHasEntityResolver2;
    public
      constructor Create; virtual;
      destructor Destroy; override;
    end;

  TExpatHashedFeatures = class(TExpatFeatures)
    protected
      class function PerfectFeatureHash(const Feature: SAXString): Longword; virtual;
    public
      class function GetFeature(Reader: TExpatXMLReader;
        const Name: SAXString): Boolean; override;
      class procedure SetFeature(Reader: TExpatXMLReader;
        const Name: SAXString; Value: Boolean); override;
    end;

  IExpatLexicalHandlerProperty = interface(IInterfaceProperty) end;
  IExpatDeclHandlerProperty = interface(IInterfaceProperty) end;
  IExpatDefaultHandlerProperty = interface(IInterfaceProperty) end;

  TExpatCoreProperties = class(TExpatProperties, IExpatLexicalHandlerProperty,
    IExpatDeclHandlerProperty, IExpatDefaultHandlerProperty)
    protected
      { IExpatLexicalHandlerProperty }
      function LexicalQueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
      function IExpatLexicalHandlerProperty.QueryInterface = LexicalQueryInterface;
      function GetLexicalName: SAXString;
      function IExpatLexicalHandlerProperty.GetName = GetLexicalName;
      function GetLexicalValue: IUnknown;
      function IExpatLexicalHandlerProperty.GetValue = GetLexicalValue;
      procedure SetLexicalValue(const PropVal: IUnknown);
      procedure IExpatLexicalHandlerProperty.SetValue = SetLexicalValue;
      { IExpatDeclHandlerProperty }
      function DeclQueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
      function IExpatDeclHandlerProperty.QueryInterface = DeclQueryInterface;
      function GetDeclName: SAXString;
      function IExpatDeclHandlerProperty.GetName = GetDeclName;
      function GetDeclValue: IUnknown;
      function IExpatDeclHandlerProperty.GetValue = GetDeclValue;
      procedure SetDeclValue(const PropVal: IUnknown);
      procedure IExpatDeclHandlerProperty.SetValue = SetDeclValue;
      { IExpatDefaultHandlerProperty }
      function DefaultQueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
      function IExpatDefaultHandlerProperty.QueryInterface = DefaultQueryInterface;
      function GetDefaultName: SAXString;
      function IExpatDefaultHandlerProperty.GetName = GetDefaultName;
      function GetDefaultValue: IUnknown;
      function IExpatDefaultHandlerProperty.GetValue = GetDefaultValue;
      procedure SetDefaultValue(const PropVal: IUnknown); 
      procedure IExpatDefaultHandlerProperty.SetValue = SetDefaultValue;
    end;

  { TExpatNativeParseError does not own FLocator!
    Warning: do not access ISAXParseError without setting valid Locator }
  TExpatParseError = class(TContainedObject, ISAXError, ISAXParseError,
    IExpatParseError)
    private
      FMessagePtr: PSAXChar;
      FMessageStr: SAXString;
      FLocator: TExpatLocator;
      FError: Exception;
    protected
      procedure SetLocator(Value: TExpatLocator);
      property Locator: TExpatLocator read FLocator implements ISAXParseError;
      { ISAXError }
      function GetMessage: PSAXChar;
      function GetNativeError: IUnknown;
      { IExpatParseError }
      function GetError: Exception;
    public
      procedure Init(Message: PSAXChar; Locator: TExpatLocator); overload;
      procedure Init(Error: Exception; Locator: TExpatLocator); overload;
      destructor Destroy; override;
    end;

  { TExpatNativeParseError does not own FError!
    Warning: do not access ISAXParseError without having valid Locator }
  TExpatNativeParseError = class(TContainedObject, ISAXError, ISAXParseError,
    IExpatParseError, IExpatNativeParseError)
    private
      FMessage: SAXString;
      FError: ESAXExpatParse;
    protected
      function GetLocator: TExpatLocator;
      property Locator: TExpatLocator read GetLocator implements ISAXParseError;
      { ISAXError }
      function GetNativeError: IUnknown;
      function GetMessage: PSAXChar;
      { IExpatParseError }
      function GetError: Exception;
      { IExpatNativeParseError }
      function GetErrorCode: Integer;
    public
      procedure Init(Error: ESAXExpatParse);
    end;

  { Convenience component - dropping it on a form or data module
    at design time adds KDSSAXExpat to the uses clause automatically }
  TSAXExpat = class(TComponent)
    private
      function GetVendor: string;
    published
      property Vendor: string read GetVendor;
    end;

// call this whenever the memory manager changes
procedure UpdateMemoryManager;


implementation

uses SysConst, KDSUtils;

{ memory management functions that can be used by Expat }

var
  MemManager: TMemoryManager = (
    GetMem: SysGetMem;
    FreeMem: SysFreeMem;
    ReallocMem: SysReallocMem);

procedure UpdateMemoryManager;
  begin
  if IsMemoryManagerSet then
    GetMemoryManager(MemManager);
  end;

function XMLAlloc(Size: Integer): Pointer; cdecl;
  begin
  Result := MemManager.GetMem(Size);
  end;

function XMLRealloc(Ptr: Pointer; Size: Integer): Pointer; cdecl;
  begin
  if Ptr = nil then
    Result := MemManager.GetMem(Size)
  else
    Result := MemManager.ReallocMem(Ptr, Size);
  end;

procedure XMLFree(Ptr: Pointer); cdecl;
  begin
  if Ptr <> nil then MemManager.FreeMem(Ptr);
  end;

const
  MMSuite: TXMLMemoryHandlingSuite = (
    MallocFcn: XMLAlloc;
    ReallocFcn: XMLReAlloc;
    FreeFcn: XMLFree);

{ Precondition: Name <> nil
  Notes: - expect triplet in Name only in case of prefix encountered
         - SAXChar dictates the interpretation of TXMLChar - see SAX_WIDESTRINGS
         - QNameBuf is passed as pre-allocated buffer (length in QNameBufLen)
           and can be returned in QName (re-allocated, if necessary)            }
procedure SAXParseName(Name: PXMLChar; out Uri: PSAXChar; out UriLen: Integer;
  out LocalName: PSAXChar; out LocalNameLen: Integer; out QName: PSAXChar;
  out QNameLen: Integer; var QNameBuf: PSAXChar; var QNameBufLen: Integer);
  var
    TmpPChar: PSAXChar;
    NameLen: Integer;
  begin
  TmpPChar := Name;
  // look for end of Uri: first NS separator
  while (TmpPChar^ <> XML_NSSEP) and (TmpPChar^ <> XML_NULLCHAR) do
    Inc(TmpPChar);
  NameLen := TmpPChar - Name;
  if TmpPChar^ = XML_NULLCHAR then  // not a qualified name
    begin
    Uri := nil;
    UriLen := 0;
    LocalName := Name;
    QName := Name;
    LocalNameLen := NameLen;
    QNameLen := NameLen;
    end
  else                              // a namespace separator was found
    begin
    Uri := Name;
    UriLen := NameLen;
    Inc(TmpPChar);
    LocalName := TmpPChar;
    // look for end of LocalName: second NS separator could be found
    while (TmpPChar^ <> XML_NSSEP) and (TmpPChar^ <> XML_NULLCHAR) do
      Inc(TmpPChar);
    NameLen := TmpPChar - LocalName;
    LocalNameLen := NameLen;
    if TmpPChar^ = XML_NULLCHAR then  // no prefix
      begin
      QName := LocalName;
      QNameLen := NameLen;
      end
    else                              // we have a prefix
      begin
      // advance from NS separator to start of prefix
      Inc(TmpPChar);
      Name := TmpPChar; // save start of prefix
      // end of string is end of Prefix
      while TmpPChar^ <> XML_NULLCHAR do
        Inc(TmpPChar);
      NameLen := NameLen + (TmpPChar - Name) + 1;
      if NameLen > QNameBufLen then
        begin
        ReallocMem(QNameBuf, NameLen * SizeOf(SAXChar));
        QNameBufLen := NameLen;
        end;
      TmpPChar := QNameBuf;
      QName := TmpPChar;
      QNameLen := NameLen;
      // return pre-allocated buffer, with QName copied in:
      // first, copy Prefix to QName
      while Name^ <> XML_NULLCHAR do   // Name points to start of Prefix
        begin
        TmpPChar^ := Name^;
        Inc(Name);
        Inc(TmpPChar);
        end;
      // copy colon (':')
      TmpPChar^ := XML_COLON;
      Inc(TmpPChar);
      // copy LocalName
      Name := LocalName;
      while Name^ <> XML_NSSEP do
        begin
        TmpPChar^ := Name^;
        Inc(Name);
        Inc(TmpPChar);
        end;
      end;
    end;
  end;

procedure SAXParseNameUriLocal(Name: PXMLChar; out Uri: PSAXChar;
  out UriLen: Integer; out LocalName: PSAXChar; out LocalNameLen: Integer);
  var
    TmpPChar: PSAXChar;
    TmpLen: Integer;
  begin
  TmpPChar := Name;
  // look for end of Uri: first NS separator
  while (TmpPChar^ <> XML_NSSEP) and (TmpPChar^ <> XML_NULLCHAR) do
    Inc(TmpPChar);
  TmpLen := TmpPChar - Name;
  if TmpPChar^ = XML_NULLCHAR then  // no URI found
    begin
    Uri := nil;
    UriLen := 0;
    LocalName := Name;
    LocalNameLen := TmpLen;
    end
  else                              // a namespace separator was found
    begin
    Uri := Name;
    UriLen := TmpLen;
    Inc(TmpPChar);
    LocalName := TmpPChar;
    // look for end of LocalName: second NS separator could be found
    while (TmpPChar^ <> XML_NSSEP) and (TmpPChar^ <> XML_NULLCHAR) do
      Inc(TmpPChar);
    LocalNameLen := TmpPChar - LocalName;
    end;
  end;

procedure SAXParseNameUri(Name: PXMLChar; out Uri: PSAXChar; out UriLen: Integer);
  var
    TmpPChar: PSAXChar;
  begin
  TmpPChar := Name;
  // look for end of Uri: first NS separator
  while (TmpPChar^ <> XML_NSSEP) and (TmpPChar^ <> XML_NULLCHAR) do
    Inc(TmpPChar);
  if TmpPChar^ <> XML_NULLCHAR then  // Uri found
    begin
    Uri := Name;
    UriLen := TmpPChar - Name;
    end
  else
    begin
    Uri := nil;
    UriLen := 0;
    end;
  end;

procedure SAXParseNameLocal(Name: PXMLChar; out LocalName: PSAXChar;
  out LocalNameLen: Integer);
  var
    TmpPChar: PSAXChar;
  begin
  TmpPChar := Name;
  // look for first NS separator
  while (TmpPChar^ <> XML_NSSEP) and (TmpPChar^ <> XML_NULLCHAR) do
    Inc(TmpPChar);
  if TmpPChar^ <> XML_NULLCHAR then  // namespace separator found
    begin
    Inc(TmpPChar);
    Name := TmpPChar;  // save start of LocalName
    // look for end of LocalName - second NS separator could be found
    while (TmpPChar^ <> XML_NSSEP) and (TmpPChar^ <> XML_NULLCHAR) do
      Inc(TmpPChar);
    end;
  LocalName := Name;
  LocalNameLen := TmpPChar - Name;
  end;

procedure SaxParseNameQual(Name: PXMLChar; out QName: PSAXChar;
  out QNameLen: Integer; var QNameBuf: PSAXChar; var QNameBufLen: Integer);
  var
    TmpPChar, LocalName: PSAXChar;
    NameLen: Integer;
  begin
  TmpPChar := Name;
  // look for end of Uri: first NS separator
  while (TmpPChar^ <> XML_NSSEP) and (TmpPChar^ <> XML_NULLCHAR) do
    Inc(TmpPChar);
  if TmpPChar^ = XML_NULLCHAR then  // not a qualified name, QName = LocalName
    begin
    QName := Name;
    QNameLen := TmpPChar - Name;;
    end
  else                              // a namespace separator was found
    begin
    Inc(TmpPChar);
    LocalName := TmpPChar;  // save start of LocalName
    // look for end of LocalName: second NS separator could be found
    while (TmpPChar^ <> XML_NSSEP) and (TmpPChar^ <> XML_NULLCHAR) do
      Inc(TmpPChar);
    NameLen := TmpPChar - LocalName;
    if TmpPChar^ = XML_NULLCHAR then  // no prefix
      begin
      QName := LocalName;
      QNameLen := NameLen;
      end
    else                              // we have a prefix
      begin
      // advance from NS separator to start of prefix
      Inc(TmpPChar);
      Name := TmpPChar;  // save start of prefix
      // end of string is end of Prefix
      while TmpPChar^ <> XML_NULLCHAR do
        Inc(TmpPChar);
      NameLen := NameLen + (TmpPChar - Name) + 1;
      if NameLen > QNameBufLen then
        begin
        ReallocMem(QNameBuf, NameLen * SizeOf(SAXChar));
        QNameBufLen := NameLen;
        end;
      TmpPChar := QNameBuf;
      QName := TmpPChar;
      QNameLen := NameLen;
      // now, build QName: i.e. first, copy Prefix
      while Name^ <> XML_NULLCHAR do  // Name points to start of Prefix
        begin
        TmpPChar^ := Name^;
        Inc(Name);
        Inc(TmpPChar);
        end;
      // copy colon (':')
      TmpPChar^ := XML_COLON;
      Inc(TmpPChar);
      // copy LocalName
      while LocalName^ <> XML_NSSEP do
        begin
        TmpPChar^ := LocalName^;
        Inc(LocalName);
        Inc(TmpPChar);
        end;
      end;
    end;
  end;

procedure WriteModelNode(ModelStream: TXMLStringStream; const ModelNode: TXMLContent);
  const
    Empty: SAXString = 'EMPTY';
    Any: SAXString = 'ANY';
    LPCData: SAXString = '(#PCDATA';
    LPar: PSAXChar = '(';
    RPar: PSAXChar = ')';
    Com: PSAXChar = ',';
    Bar: PSAXChar = '|';
    Quest: PSAXChar = '?';
    Ast: PSAXChar = '*';
    Plus: PSAXChar = '+';
  var
    Indx: Integer;
    SepChar: PSAXChar;
  begin
  case ModelNode.Type_ of
    XML_CTYPE_EMPTY: ModelStream.WriteChars(PSAXChar(Empty), Length(Empty));
    XML_CTYPE_ANY: ModelStream.WriteChars(PSAXChar(Any), Length(Any));
    XML_CTYPE_NAME: ModelStream.WriteNullTerminated(ModelNode.Name);
    XML_CTYPE_CHOICE, XML_CTYPE_SEQ:
      begin
      ModelStream.WriteChars(LPar, 1);
      if ModelNode.Type_ = XML_CTYPE_SEQ then
        SepChar := Com
      else
        SepChar := Bar;
      Assert(ModelNode.NumChildren > 0);
      WriteModelNode(ModelStream, ModelNode.Children^[0]);
      for Indx := 1 to ModelNode.NumChildren - 1 do
        begin
        ModelStream.WriteChars(SepChar, 1);
        WriteModelNode(ModelStream, ModelNode.Children^[Indx]);
        end;
      ModelStream.WriteChars(RPar, 1);
      end;
    XML_CTYPE_MIXED:
      begin
      ModelStream.WriteChars(PSAXChar(LPCData), Length(LPCData));
      for Indx := 0 to Integer(ModelNode.NumChildren) - 1 do
        begin
        ModelStream.WriteChars(Bar, 1);
        WriteModelNode(ModelStream, ModelNode.Children^[Indx]);
        end;
      ModelStream.WriteChars(RPar, 1);
      end;
    end;
  case ModelNode.Quant of
    XML_CQUANT_OPT: ModelStream.WriteChars(Quest, 1);
    XML_CQUANT_REP: ModelStream.WriteChars(Ast, 1);
    XML_CQUANT_PLUS: ModelStream.WriteChars(Plus, 1);
    end;
  end;

type
  TModelStreamCracker = class(TXMLStringStream);

procedure ElementDeclHandler(UserData: Pointer; ElName: PXMLChar;
  Model: PXMLContent); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    ElNameLen: Integer;
  begin
  // not necessary - callback only set when DeclHandler <> nil
  // if XMLReader.FDeclHandler = nil then Exit;
  with XMLReader, FParseData do
    try
      ContentModel := Model;
      if LongBool(ElName) then ElNameLen := -1 else ElNameLen := 0;
      if ContentStream = nil then
        ContentStream := TXMLStringStream.Create;
      ContentStream.StringPosition := 1;
      ContentStream.StringLength := 0;
      WriteModelNode(ContentStream, Model^);
      FDeclHandler.ElementDecl(ElName, ElNameLen,
        TModelStreamCracker(ContentStream).Data, ContentStream.StringLength);
    finally
      // was allocated by Expat, but we own it and must de-allocate it
      XMLFreeContentModel(FEntityParseData.Parser, Model);
      ContentModel := nil;
      end;
  end;

{ Note:
  DTDs don't know about namespaces, ElName, AttName have to be taken literally }
procedure AttlistDeclHandler(UserData: Pointer; ElName, AttName, AttType,
  Dflt: PXMLChar; IsRequired: Integer); cdecl;
  const
    Fixed = SAXString('#FIXED');
    Required = SAXString('#REQUIRED');
    Implied = SAXString('#IMPLIED');
    None = SAXString('');
  var
    XMLReader: TExpatXMLReader absolute UserData;
    ElLen, AttLen, TypeLen, DfltLen: Integer;
    DfltType: SAXString;
  begin
  // not necessary - callback only set when DeclHandler <> nil
  // if XMLReader.FDeclHandler = nil then Exit;
  if LongBool(ElName) then ElLen := -1 else ElLen := 0;
  if LongBool(AttName) then AttLen := -1 else AttLen := 0;
  if LongBool(AttType) then TypeLen := -1 else TypeLen := 0;
  if LongBool(Dflt) then DfltLen := -1 else DfltLen := 0;
  // determine default type
  if LongBool(IsRequired) then
    begin
    if Dflt = nil then DfltType := Required
    else DfltType := Fixed;
    end
  else
    begin
    if Dflt = nil then DfltType := Implied
    else DfltType := None;
    end;
  with XMLReader do
    begin
    Attributes.AttrDecls.AddDecl(ElName, AttName, AttType,
      PSAXChar(DfltType), Dflt);
    FDeclHandler.AttributeDecl(ElName, ElLen, AttName, AttLen, AttType,
      TypeLen, PSAXChar(DfltType), Length(DfltType), Dflt, DfltLen);
    end;
  end;

{ Notes
  - Version and Encoding are reported through the new ILocator2 interface,
    Standalone is reported through the "is-standalone" feature in SAX2
  - Called when the XML or text declarations are parsed. Standalone will be 1
    if the document is declared standalone, 0 if it is declared not to be
    standalone, or -1 if the standalone clause was omitted                   }
procedure XmlDeclHandler(UserData: Pointer; Version, Encoding: PXMLChar;
  Standalone: Integer); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
  begin
  with XMLReader do
    begin
    // the docs say that Version = nil for Text declarations (which occur
    // in external entities), but this does not seem to be correct, so we
    // use a different test to check if we have an XML or text declaration
    if FEntityParseData.Parent = nil then FParseData.Standalone := Standalone;
    FEntityParseData.FVersion := SAXString(Version);
    // the declaration's encoding cannot override the input source!
    if FEntityParseData.Encoding = '' then  // no encoding set by input source
      FEntityParseData.FEncoding := SAXString(Encoding);
    end;
  end;

{ the Atts argument is only valid for this call! }
procedure StartElementHandler(UserData: Pointer; ElName: PXMLChar;
  const Atts: TAttrs); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    SpecAttrCount: Integer;
    Uri, LocalName, QName: PSAXChar;
    UriLen, LocalLen, QNameLen: Integer;
  begin
  with XMLReader do
    begin
    // not necessary - callback only set when ContentHandler <> nil
    // if FContentHandler = nil then Exit;
    SpecAttrCount := XMLGetSpecifiedAttributeCount(FEntityParseData.Parser);
    SAXParseName(ElName, Uri, UriLen, LocalName, LocalLen, QName, QNameLen,
      FParseData.NameBuf, FParseData.NameBufLen);
    Attributes.Initialize(QName, QNameLen, Atts, SpecAttrCount);
    FContentHandler.StartElement(Uri, UriLen, LocalName, LocalLen, QName,
      QNameLen, IBufferedAttributes2(Attributes)); // enable hard cast to IAttributes2
    end;
  end;

procedure EndElementHandler(UserData: Pointer; ElName: PXMLChar); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    Uri, LocalName, QName: PSAXChar;
    UriLen, LocalLen, QNameLen: Integer;
  begin
  with XMLReader do
    begin
    // not necessary - callback only set when ContentHandler <> nil
    // if ContentHandler = nil then Exit;
    SAXParseName(ElName, Uri, UriLen, LocalName, LocalLen, QName, QNameLen,
      FParseData.NameBuf, FParseData.NameBufLen);
    FContentHandler.EndElement(Uri, UriLen, LocalName, LocalLen,
      QName, QNameLen);
    end;
  end;

{ S is not 0 terminated. }
procedure CharacterDataHandler(UserData: Pointer; S: PXMLChar; Len: Integer); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
  begin
  // not necessary - callback only set when ContentHandler <> nil
  // if XMLReader.FContentHandler = nil then Exit;
  XMLReader.FContentHandler.Characters(S, Len);
  end;

{ Target and Data are 0 terminated }
procedure ProcessingInstructionHandler(UserData: Pointer; Target, Data: PXMLChar); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    TargetLen, DataLen: Integer;
  begin
  // not necessary - callback only set when ContentHandler <> nil
  // if XMLReader.FContentHandler = nil then Exit;
  if LongBool(Target) then TargetLen := -1 else TargetLen := 0;
  if LongBool(Data) then DataLen := -1 else DataLen := 0;
  XMLReader.FContentHandler.ProcessingInstruction(Target, TargetLen, Data, DataLen);
  end;

{ Data is 0 terminated }
procedure CommentHandler(UserData: Pointer; Data: PXMLChar); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    DataLen: Integer;
  begin
  // not necessary - callback only set when LexicalHandler <> nil
  // if XMLReader.FLexicalHandler = nil then Exit;
  if LongBool(Data) then DataLen := -1 else DataLen := 0;
  XMLReader.FLexicalHandler.Comment(Data, DataLen);
  end;

procedure StartCDataSectionHandler(UserData: Pointer); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
  begin
  // not necessary - callback only set when LexicalHandler <> nil
  // if XMLReader.FLexicalHandler = nil then Exit;
  XMLReader.FLexicalHandler.StartCDATA;
  end;

procedure EndCDataSectionHandler(UserData: Pointer); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
  begin
  // not necessary - callback only set when LexicalHandler <> nil
  // if XMLReader.FLexicalHandler = nil then Exit;
  XMLReader.FLexicalHandler.EndCDATA;
  end;

{ Precondition: ContentHandler = nil, DefaultDataHandler <> nil
  Note: We are using this to report unhandled data             }
procedure DefaultXMLHandler(UserData: Pointer; S: PXMLChar; Len: Integer); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
  begin
  // not necessary - callback only set when DefaultDataHandler <> nil
  // if XMLReader.FDefaultDataHandler = nil then Exit;
  XMLReader.FDefaultDataHandler.UnhandledData(S, Len);
  end;

{ Note: not sure what to do with HasInternalSubset (how to pass or store) //kw }
procedure StartDoctypeDeclHandler(UserData: Pointer; DocTypeName, SysId,
  PubId: PXMLChar; HasInternalSubset: Integer); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    DocTypeLen: Integer;
    PublicId, SystemId: SAXString;
  begin
  // normalize empty strings
  if (PubId <> nil) and (PubId^ = XML_NULLCHAR) then PubId := nil;
  if (SysId <> nil) and (SysId^ = XML_NULLCHAR) then SysId := nil;
  PublicId := SAXString(PubId);
  SystemId := SAXString(SysId);
  if LongBool(DocTypeName) then DocTypeLen := -1 else DocTypeLen := 0;

  with XMLReader do
    begin
    FParseData.HasDocTypeDecl := True;
    if SysId = nil then with FParseData do
      begin
      ForeignDTDSource := ResolveForeignDTD(DocTypeName, DocTypeLen);
      if ForeignDTDSource <> nil then
        begin
        PublicId := ForeignDTDSource.PublicId;
        SystemId := ForeignDTDSource.SystemId;
        end;
      end;
    // if PubId = nil *and* SysId = nil, then we don't have an external subset,
    // but ExternalEntityRefHandler may still be called for a foreign DTD,
    // so maybe we should store the entity
    ParamEntityDecls.AddDecl(XML_DTDNAME, PublicId, SystemId);
    if FLexicalHandler = nil then Exit;
    // we report the "declared", not absolutized, system identifier
    FLexicalHandler.StartDTD(DocTypeName, DocTypeLen, PSAXChar(PublicId),
      Length(PublicId), PSAXChar(SystemId), Length(SystemId));
    end;
  end;

procedure EndDoctypeDeclHandler(UserData: Pointer); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
  begin
  if XMLReader.FLexicalHandler = nil then Exit;
  XMLReader.FLexicalHandler.EndDTD;
  end;

{ Note: Not sure what to do with Base - SAX2 expects absolute URIs //kw }
procedure EntityDeclHandler(UserData: Pointer; EntityName: PXMLChar;
  IsParameterEntity: Integer; Value: PXMLChar; ValueLength: Integer;
  Base, SysId, PubId, NotationName: PXMLChar); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    EntLen, SysLen, PubLen: Integer;
    TmpBuf: PSAXChar;
    ParEntName, PublicId, SystemId: SAXString;
  begin
  // normalize empty strings
  if (PubId <> nil) and (PubId^ = XML_NULLCHAR) then PubId := nil;
  if (SysId <> nil) and (SysId^ = XML_NULLCHAR) then SysId := nil;
  // if (Base <> nil) and (Base^ = XML_NULLCHAR) then Base := nil;

  with XMLReader do
    begin
    if LongBool(EntityName) then
      EntLen := SAXStrLen(EntityName) else EntLen := 0;
    // for parameter entity, prefix name with '%'
    if Longbool(IsParameterEntity) then
      begin
      SysLen := EntLen; // borrow Syslen for a different purpose here
      Inc(EntLen);
      SetLength(ParEntName, EntLen);
      TmpBuf := PSAXChar(ParEntName);
      TmpBuf^ := XML_PERCENT;
      while SysLen > 0 do
        begin
        Inc(TmpBuf);
        TmpBuf^ := EntityName^;
        Inc(EntityName);
        Dec(SysLen);
        end;
      PublicId := SAXString(PubId);
      PubLen := Length(PublicId);
      SystemId := SAXString(SysId);
      SysLen := Length(SystemId);
      if SysLen > 0 then  // external entities *must* have system id
        ParamEntityDecls.AddDecl(ParEntName, PublicId, SystemId);
      EntityName := PSAXChar(ParEntName);
      end
    else
      begin
      if LongBool(SysId) then SysLen := -1 else SysLen := 0;
      if LongBool(PubId) then PubLen := -1 else PubLen := 0;
      end;
    if NotationName <> nil then // unparsed entity declaration
      begin
      if FDTDHandler = nil then Exit;
      //kw should be able to absolutize (resolve) systemId using BaseURI
      FDTDHandler.UnparsedEntityDecl(EntityName, EntLen, PubId, PubLen,
        SysId, SysLen, NotationName, -1);
      end
    else
      begin
      if FDeclHandler = nil then Exit;
      if Value <> nil then
        FDeclHandler.InternalEntityDecl(EntityName, EntLen, Value, ValueLength)
      else
        //kw should be able to absolutize (resolve) systemId using BaseURI
        FDeclHandler.ExternalEntityDecl(EntityName, EntLen, PubId, PubLen,
          SysId, SysLen);
      end;
    end;
  end;

{ obsolete, handled by the EntityDeclHandler }
procedure UnparsedEntityDeclHandler(UserData: Pointer; EntityName,
  Base, SysId, PubID, NotationName: PXMLChar); cdecl;
  begin
  //
  end;

procedure NotationDeclHandler(UserData: Pointer; NotationName,
  Base, SysId, PubId: PXMLChar); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    NotLen, PubLen, SysLen: Integer;
  begin
  // not necessary - callback only set when DTDHandler <> nil
  // if XMLReader.FDTDHandler = nil then Exit;
  if LongBool(NotationName) then NotLen := -1 else NotLen := 0;
  if LongBool(PubId) then PubLen := -1 else PubLen := 0;
  if LongBool(SysId) then SysLen := -1 else SysLen := 0;
  //kw should be able to absolutize (resolve) systemId using BaseURI
  XMLReader.FDTDHandler.NotationDecl(NotationName, NotLen, PubId, PubLen,
    SysId, SysLen);
  end;

procedure StartNamespaceDeclHandler(UserData: Pointer; Prefix, Uri: PXMLChar); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    PrefixLen, UriLen: Integer;
  begin
  // not necessary - callback only set when ContentHandler <> nil
  // if XMLReader.FContentHandler = nil then Exit;
  if LongBool(Prefix) then PrefixLen := -1 else PrefixLen := 0;
  if LongBool(Uri) then UriLen := -1 else UriLen := 0;
  XMLReader.FContentHandler.StartPrefixMapping(Prefix, PrefixLen, Uri, UriLen);
  end;

procedure EndNamespaceDeclHandler(UserData: Pointer; Prefix: PXMLChar); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    PrefixLen: Integer;
  begin
  // not necessary - callback only set when ContentHandler <> nil
  // if XMLReader.FContentHandler = nil then Exit;
  if LongBool(Prefix) then PrefixLen := -1 else PrefixLen := 0;
  XMLReader.FContentHandler.EndPrefixMapping(Prefix, PrefixLen);
  end;

{ Note: This determines if the parser returns an XML_ERROR_NOT_STANDALONE
  error, when it encounters an external subset or a reference to a parameter
  entity, but does have standalone set to "yes" in an XML declaration    }
function NotStandaloneHandler(UserData: Pointer): Integer; cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
  begin
  if XMLReader.StandaloneError then
    Result := 0  // this causes the parser to return an error
  else
    Result := 1;
  end;

{ Extracts entity name from Expat context string, returns (nil, 0) if not found;
  See expat.h for documentation of Context structure;
  Precondition: Context must be <> nil and null terminated                                }
procedure GetEntityName(Context: PXMLChar; out EntName: PSAXChar;
  out EntNameLen: Integer);
  var
    IsName: LongBool absolute EntNameLen;  // use EntNameLen as internal flag
  begin
  repeat
    IsName := True;
    EntName := Context;
    while (Context^ <> XML_NULLCHAR) and (Context^ <> XML_CONTEXTSEP) do
      begin
      // if we have an "=", we don't have a name
      if Context^ = XML_EQUALS then
        begin
        // skip to end of token
        repeat
          Inc(Context)
          until (Context^ = XML_NULLCHAR) or (Context^ = XML_CONTEXTSEP);
        IsName := False;
        Break;
        end;
      Inc(Context);
      end;
    // at this point, Context^ = XML_NULLCHAR or XML_CONTEXTSEP
    if IsName then
      begin
      EntNameLen := Context - EntName;
      Exit;
      end
    else if Context^ = XML_NULLCHAR then
      Break
    else
      Inc(Context);
    until False;
  // EntNameLen := 0;  not required, since Ord(False) = 0 for LongBool
  EntName := nil;
  end;

{ Note: not sure what to do with Base - see XMLExternalEntityParserCreate //kw }
{ It seems that for the external [dtd] subset and for parameter entities,
  Context = nil, but for general entitities it contains the name - look for
  first token (separated by form feeds) that does not contain an '='           }
function ExternalEntityRefHandler(ParentParser: TXMLParser; Context, Base,
  SysId, PubId: PXMLChar): Integer; cdecl;
  function CannotResolveStr(Msg: SAXString): SAXString;
    begin
    // must cast PXMLChar to SAXString, because a nil pointer would give an AV
    Result := Format(SCannotResolveEntity, [SAXString(PubId), SAXString(SysId)])
      + SAXString(#13#10) + Msg;
    end;
  var
    XMLReader: TExpatXMLReader;
    InSource: IInputSource;
    StreamSource: IStreamInputSource;
    ReaderSource: IReaderInputSource;
    EntName: PSAXChar;
    EntNameLen, PubIdLen, SysIdLen: Integer;
    ParEntName: SAXString;
    Error: Exception;
    TmpPublicId, TmpSystemId: SAXString;
  begin
  // by default we assume that the calling parser can continue
  Result := Ord(XML_STATUS_OK);
  // normalize empty strings
  if (PubId <> nil) and (PubId^ = XML_NULLCHAR) then PubId := nil;
  if (SysId <> nil) and (SysId^ = XML_NULLCHAR) then SysId := nil;
  
  XMLReader := TExpatXMLReader(XMLGetUserData(ParentParser));
  with XMLReader do
    begin
    if Context <> nil then
      begin
      GetEntityName(Context, EntName, EntNameLen);
      if not FExternalGeneral then
        begin
        if FContentHandler <> nil then
          FContentHandler.SkippedEntity(EntName, EntNameLen);
        Exit;
        end;
      end
    else
      begin
      EntName := nil;
      if SysId = nil then  // use foreign DTD
        begin
        EntName := PSAXChar(XML_DTDNAME);
        EntNameLen := Length(XML_DTDNAME);
        if FParseData.HasDocTypeDecl then
          InSource := FParseData.ForeignDTDSource
        else
          begin
          InSource := ResolveForeignDTD(nil, 0);
          FParseData.ForeignDTDSource := InSource;
          end;
        if InSource <> nil then
          begin
          TmpPublicId := InSource.PublicId;
          TmpSystemId := InSource.SystemId;
          end
        else
          Exit;
        end
      // don't have name for parameter entity, have to look it up in hash table
      else if ParamEntityDecls.FindDecl(PubId, SysId) then
        begin
        ParamEntityDecls.GetDecl(ParEntName);
        EntName := PSAXChar(ParEntName);
        EntNameLen := Length(ParEntName);
        end;
      if EntName <> nil then
        begin
        if not FExternalParameter then
          begin
          if FContentHandler <> nil then
            FContentHandler.SkippedEntity(EntName, EntNameLen);
          Exit;
          end;
        end
      else
        begin
        // Expat should not report an entity here if it hasn't read it's
        // declaration, since it would not know it's external id then
        if LongBool(PubId) then PubIdLen := -1 else PubIdLen := 0;
        if LongBool(SysId) then SysIdLen := -1 else SysIdLen := 0;
        Error := ESAXExpat.CreateFmt(SUnknownEntity,
          [PSAXCharToString(PubId, PubIdLen), PSAXCharToString(SysId, SysIdLen)]);
        try
          CallErrorHandler(setError, Error);
        finally
          Error.Free;
          end;
        Exit;
        end;
      end;

    // if we got that far, then we don't want to skip the entity
    if FLexicalHandler <> nil then
      begin
      // a foreign DTD without DocType declaration would not trigger a
      // StartDocTypeDecl call-back from Expat, so we call StartDTD here
      if (SysId = nil) and not FParseData.HasDocTypeDecl then
        FLexicalHandler.StartDTD(
          PSAXChar(FForeignDoctypeName), Length(FForeignDoctypeName),
          PSAXChar(TmpPublicId), Length(TmpPublicId),
          PSAXChar(TmpSystemId), Length(TmpSystemId));
      FLexicalHandler.StartEntity(EntName, EntNameLen);
      end;

    // input source not yet resolved, except for foreign DTD (SysId = nil)
    if InSource = nil then
      begin
      if FEntityResolver <> nil then
        begin
        InSource := ResolveExternalEntity(EntName, EntNameLen, PubId, Base, SysId);
        if InSource = nil then
          begin
          CallErrorHandler(setError, CannotResolveStr(''));
          Exit;
          end;
        end
      else
        begin
        CallErrorHandler(setError, CannotResolveStr(SNoEntityResolver));
        Exit;
        end;
      end;

    // we have an input source - set up context record for child entity
    if InSource.QueryInterface(IStreamInputSource, StreamSource) = S_OK then
      begin
      if not PushChildEntityParseData(ParentParser, EntName, EntNameLen,
        Context, StreamSource, True) then Exit;
      end
    else if InSource.QueryInterface(IReaderInputSource, ReaderSource) = S_OK then
      begin
      if not PushChildEntityParseData(ParentParser, EntName, EntNameLen,
        Context, ReaderSource, False) then Exit;
      end
    else
      begin
      CallErrorHandler(setError, SInvalidInputSource);
      Exit;
      end;

    case ContinueEntityParsing of
      dpsFinished: FinishExternalEntityParsing;
      // must suspend parent parser as well - don't check return value
      dpsSuspended: XMLStopParser(ParentParser, True);
      dpsFatalError: Result := Ord(XML_STATUS_ERROR);
      // must abort parent parser as well - don't check return value
      dpsAborted: XMLStopParser(ParentParser, False);
      end;
    end;
  end;

procedure SkippedEntityHandler(UserData: Pointer; EntityName: PXMLChar;
  IsParameterEntity: Integer); cdecl;
  var
    XMLReader: TExpatXMLReader absolute UserData;
    TmpBuf: PSAXChar;
    EntNameLen, TmpLen: Integer;
  begin
  // not necessary - callback only set when ContentHandler <> nil
  // if XMLReader.FContentHandler = nil then Exit;
  if LongBool(EntityName) then
    EntNameLen := SAXStrLen(EntityName)
  else
    EntNameLen := 0;
  with XMLReader do
    begin
    if LongBool(IsParameterEntity) then
      begin
      TmpLen := EntNameLen;
      Inc(EntNameLen);  // make space for percent sign
      with FParseData do
        begin
        if NameBufLen < EntNameLen then
          begin
          ReallocMem(NameBuf, EntNameLen * SizeOf(SAXChar));
          NameBufLen := EntNameLen;
          end;
        TmpBuf := NameBuf;
        TmpBuf^ := XML_PERC;
        while TmpLen > 0 do
          begin
          Inc(TmpBuf);
          TmpBuf^ := EntityName^;
          Inc(EntityName);
          Dec(TmpLen);
          end;
        FContentHandler.SkippedEntity(NameBuf, EntNameLen);
        end;
      end
    else
      FContentHandler.SkippedEntity(EntityName, EntNameLen);
    end;
  end;

{ Note: Currently no additional encodings are supported //kw }
function UnknownEncodingHandler(EncodingHandlerData: Pointer;
  Name: PXMLChar; Info: PXMLEncoding): Integer; cdecl;
  begin
  Result := 0;
  end;

{ TExpatProperties }

constructor TExpatProperties.Create(Reader: TExpatXMLReader);
  begin
  inherited Create(Reader);
  FReader := Reader;
  end;

{ TExpatCoreProperties }

function TExpatCoreProperties.GetLexicalValue: IUnknown;
  begin
  Result := FReader.GetLexicalHandler;
  end;

procedure TExpatCoreProperties.SetLexicalValue(const PropVal: IUnknown);
  begin
  FReader.SetLexicalHandler(PropVal as IBufferedLexicalHandler);
  end;

function TExpatCoreProperties.GetDeclValue: IUnknown;
  begin
  Result := FReader.GetDeclHandler;
  end;

procedure TExpatCoreProperties.SetDeclValue(const PropVal: IUnknown);
  begin
  FReader.SetDeclHandler(PropVal as IBufferedDeclHandler);
  end;

function TExpatCoreProperties.GetDeclName: SAXString;
  begin
  Result := DeclHandlerProperty
  end;

function TExpatCoreProperties.GetLexicalName: SAXString;
  begin
  Result := LexicalHandlerProperty;
  end;

function TExpatCoreProperties.GetDefaultName: SAXString;
  begin
  Result := DefaultHandlerProperty;
  end;

function TExpatCoreProperties.GetDefaultValue: IUnknown;
  begin
  Result := FReader.GetDefaultDataHandler;
  end;

procedure TExpatCoreProperties.SetDefaultValue(const PropVal: IUnknown);
  begin
  FReader.SetDefaultDataHandler(PropVal as IBufferedDefaultHandler);
  end;

function TExpatCoreProperties.DeclQueryInterface(const IID: TGUID; out Obj): HResult;
  begin
  if IsEqualGUID(IID, IInterfaceProperty) then
    begin
    // initialize properly, otherwise we get extra _Release calls
    Pointer(Obj) := nil;
    // this cast takes care of reference counting
    IExpatDeclHandlerProperty(Obj) := Self;
    // this would be an equivalent solution:
    // Pointer(Obj) := Pointer(IExpatLexicalHandlerProperty(Self));
    // if Pointer(Obj) <> nil then IUnknown(Obj)._Addref;
    Result := 0;
    end
  else
    Result := inherited QueryInterface(IID, Obj);
  end;

function TExpatCoreProperties.DefaultQueryInterface(const IID: TGUID; out Obj): HResult;
  begin
  if IsEqualGUID(IID, IInterfaceProperty) then
    begin
    // initialize properly, otherwise we get extra _Release calls
    Pointer(Obj) := nil;
    // this cast takes care of reference counting
    IExpatDefaultHandlerProperty(Obj) := Self;
    Result := 0;
    end
  else
    Result := inherited QueryInterface(IID, Obj);
  end;

function TExpatCoreProperties.LexicalQueryInterface(const IID: TGUID; out Obj): HResult;
  begin
  if IsEqualGUID(IID, IInterfaceProperty) then
    begin
    // initialize properly, otherwise we get extra _Release calls
    Pointer(Obj) := nil;
    // this cast takes care of reference counting
    IExpatLexicalHandlerProperty(Obj) := Self;
    Result := 0;
    end
  else
    Result := inherited QueryInterface(IID, Obj);
  end;

{ TExpatHashedFeatures }

{$Q-}  // for hash functions turn overflow checking off

{ returns unique number between 0 and 24 for the 16 features recognized;
  the numbers are:
  20:  http://xml.org/sax/features/namespaces
  11:  http://xml.org/sax/features/namespace-prefixes
  17:  http://xml.org/sax/features/validation
  23:  http://xml.org/sax/features/external-general-entities
  13:  http//xml.org/sax/features/external-parameter-entities
  16:  http//xml.org/sax/features/is-standalone
  22:  http//xml.org/sax/features/use-attributes2
   9:  http//xml.org/sax/features/use-locator2
  19:  http//xml.org/sax/features/use-entity-resolver2
   0:  http//xml.org/sax/features/resolve-dtd-uris
  10:  http//xml.org/sax/features/lexical-handler/parameter-entities
   5:  http//kd-soft.net/sax/features/parameter-entities
   2:  http//kd-soft.net/sax/features/skip-internal-entities
   3:  http//kd-soft.net/sax/features/parse-unless-standalone
  15:  http//kd-soft.net/sax/features/standalone-error
   1:  http//saxforpascal.org/features/use-reader-control             }
class function TExpatHashedFeatures.PerfectFeatureHash(const Feature: SAXString): Longword;
  const
    FeatureLen = Length(SAX.Features) + 5;
  var
    Indx: Integer;
  begin
  Result := 175;
  for Indx := FeatureLen to FeatureLen + 5 do
    Result := (Result * $F4243) xor Ord(Feature[Indx]);
  Result := Result mod 25
  end;

{$IFDEF _KDSExpat_Q+} {$Q+} {$ENDIF}  // turn overflow checking back on

type
  TExpatFeatureGetter = function(Reader: TExpatXMLReader): Boolean;
  TExpatFeatureSetter = procedure(Reader: TExpatXMLReader; Value: Boolean);

  TExpatFeatureRec = record
    Name: SAXString;
    Getter: TExpatFeatureGetter;
    Setter: TExpatFeatureSetter;
    end;
  TExpatFeatureDummy = record
    Name: SAXString;
    Getter: Pointer;
    Setter: Pointer;
    end;

const
  FeatureHigh = 24;

  // has to be CONSISTENT with PerfectFeaturehash and Feature name constants
  ExpatFeatureDummies: array[0..FeatureHigh] of TExpatFeatureDummy = (
    (Name: ResolveDTDURIsFeature;
     Getter: @TExpatXMLReader.GetResolveDTDURIs;
     Setter: nil),
    (Name: ReaderControlFeature;
     Getter: @TExpatXMLReader.GetReaderControl;
     Setter: nil), 
    (Name: SkipInternalFeature;
     Getter: @TExpatXMLReader.GetSkipInternal;
     Setter: @TExpatXMLReader.SetSkipInternal),
    (Name: ParseUnlessStandaloneFeature;
     Getter: @TExpatXMLReader.GetParseUnlessStandalone;
     Setter: @TExpatXMLReader.SetParseUnlessStandalone),
    (Name: ''; Getter: nil; Setter: nil), // not recognized
    (Name: ParameterEntitiesFeature;
     Getter: @TExpatXMLReader.GetParameterEntities;
     Setter: @TExpatXMLReader.SetParameterEntities),
    (Name: ''; Getter: nil; Setter: nil), // not recognized
    (Name: ''; Getter: nil; Setter: nil), // not recognized
    (Name: ''; Getter: nil; Setter: nil), // not recognized
    (Name: UseLocator2Feature;
     Getter: @TExpatXMLReader.GetUseLocator2;
     Setter: nil),
    (Name: LexicalParameterFeature;
     Getter: @TExpatXMLReader.GetParameterEntities;
     Setter: @TExpatXMLReader.SetParameterEntities),
    (Name: NamespacePrefixesFeature;
     Getter: @TExpatXMLReader.GetNamespacePrefixes;
     Setter: @TExpatXMLReader.SetNamespacePrefixes),
    (Name: ''; Getter: nil; Setter: nil), // not recognized
    (Name: ExternalParameterFeature;
     Getter: @TExpatXMLReader.GetExternalParameter;
     Setter: @TExpatXMLReader.SetExternalParameter),
    (Name: ''; Getter: nil; Setter: nil), // not recognized
    (Name: StandaloneErrorFeature;
     Getter: @TExpatXMLReader.GetStandaloneError;
     Setter: @TExpatXMLReader.SetStandaloneError),
    (Name: IsStandaloneFeature;
     Getter: @TExpatXMLReader.IsStandalone;
     Setter: nil),
    (Name: ValidationFeature;
     Getter: @TExpatXMLReader.GetValidation;
     Setter: @TExpatXMLReader.SetValidation),
    (Name: ''; Getter: nil; Setter: nil), // not recognized
    (Name: UseEntityResolver2Feature;
     Getter: @TExpatXMLReader.GetUseEntityResolver2;
     Setter: nil),
    (Name: NamespacesFeature;
     Getter: @TExpatXMLReader.GetNameSpaces;
     Setter: @TExpatXMLReader.SetNameSpaces),
    (Name: ''; Getter: nil; Setter: nil), // not recognized
    (Name: UseAttributes2Feature;
     Getter: @TExpatXMLReader.GetUseAttributes2;
     Setter: nil),
    (Name: ExternalGeneralFeature;
     Getter: @TExpatXMLReader.GetExternalGeneral;
     Setter: @TExpatXMLReader.SetExternalGeneral),
    (Name: ''; Getter: nil; Setter: nil)  // not recognized
    );

type
  TExpatFeatureRecs = array[0..FeatureHigh] of TExpatFeatureRec;
var
  ExpatFeatureRecs: TExpatFeatureRecs absolute ExpatFeatureDummies;

{$WARNINGS OFF}   // don't warn about uninitialized variables
class function TExpatHashedFeatures.GetFeature(Reader: TExpatXMLReader;
  const Name: SAXString): Boolean;
  var
    Indx: Integer;
    FeatureGet: TExpatFeatureGetter;
  begin
  Indx := PerfectFeatureHash(Name);
  if (Indx < Low(ExpatFeatureRecs)) or (Indx > High(ExpatFeatureRecs)) then
    NotRecognizedError(SFeatureNotRecognized, Name);
  if Name <> ExpatFeatureRecs[Indx].Name then
    NotRecognizedError(SFeatureNotRecognized, Name);
  FeatureGet := ExpatFeatureRecs[Indx].Getter;
  if Assigned(FeatureGet) then Result := FeatureGet(Reader)
  else NotSupportedError(SFeatureReadNotSupported, Name);
  end;
{$WARNINGS ON}

class procedure TExpatHashedFeatures.SetFeature(Reader: TExpatXMLReader;
  const Name: SAXString; Value: Boolean);
  var
    Indx: Integer;
    FeatureSet: TExpatFeatureSetter;
  begin
  Indx := PerfectFeatureHash(Name);
  if (Indx < Low(ExpatFeatureRecs)) or (Indx > High(ExpatFeatureRecs)) then
    NotRecognizedError(SFeatureNotRecognized, Name);
  if Name <> ExpatFeatureRecs[Indx].Name then
    NotRecognizedError(SFeatureNotRecognized, Name);
  FeatureSet := ExpatFeatureRecs[Indx].Setter;
  if Assigned(FeatureSet) then FeatureSet(Reader, Value)
  else NotSupportedError(SFeatureWriteNotSupported, Name);
  end;

{ TExpatXMLReader }

constructor TExpatXMLReader.Create;
  begin
  inherited Create;
  FNamespaces := True;
  FParameterEntities := True;
  FAttributes := CreateAttributes;
  FParamEntityDecls := TExpatParEntDecls.Create;
  FProperties := CreateProperties;
  FParseError := TExpatParseError.Create(Self);
  FNativeParseError := TExpatNativeParseError.Create(Self);
  // pre-create one instance
  FFreeEntityParseData := TEntityParseData.Create;
  end;

destructor TExpatXMLReader.Destroy;
  begin
  DisposeEntityParsedata(FFreeEntityParseData);
  DisposeEntityParsedata(FEntityParseData);
  FAttributes.Free;
  FParamEntityDecls.Free;
  FProperties.Free;
  FParseError.Free;
  FNativeParseError.Free;
  inherited Destroy;
  end;

procedure TExpatXMLReader.CallErrorHandler(ErrType: TSAXErrorType;
  const Msg: SAXString);
  begin
  FParseError.Init(PSAXChar(Msg), FParseData.Locator);
  if FErrorHandler <> nil then
    case ErrType of
      setWarning: FErrorHandler.Warning(FParseError);
      setError: FErrorHandler.Error(FParseError);
      setFatal: FErrorHandler.FatalError(FParseError);
      end;
  end;

procedure TExpatXMLReader.CallErrorHandler(ErrType: TSAXErrorType;
  var Error: Exception);
  begin
  Assert(Error <> nil);
  FParseError.Init(Error, FParseData.Locator);
  if FErrorHandler <> nil then
    begin
    try
      case ErrType of
        setWarning: FErrorHandler.Warning(FParseError);
        setError: FErrorHandler.Error(FParseError);
        setFatal: FErrorHandler.FatalError(FParseError);
        end;
    except
      // prevent Error from being freed a second time
      if ExceptObject = Error then Error := nil;
      raise;
      end;
    end;
  end;

procedure TExpatXMLReader.CallNativeErrorHandler(ErrType: TSAXErrorType;
  var Error: ESAXExpatParse);
  begin
  Assert(Error <> nil);
  FNativeParseError.Init(Error);
  if FErrorHandler <> nil then
    begin
    try
      case ErrType of
        setWarning: FErrorHandler.Warning(FNativeParseError);
        setError: FErrorHandler.Error(FNativeParseError);
        setFatal: FErrorHandler.FatalError(FNativeParseError);
        end;
    except
      // prevent Error from being freed a second time
      if ExceptObject = Error then Error := nil;
      raise;
      end;
    end;
  end;

procedure TExpatXMLReader.ConfigureExpatHandlersForParsing;
  begin
  SetDeclHandlerForParsing;         // DeclHandler callbacks
  SetLexicalHandlerForParsing;      // LexicalHandler callbacks
  SetDTDHandlerForParsing;          // DTDHandler callbacks
  SetDefaultDataHandlerForParsing;  // DefaultDataHandler callbacks
  SetContentHandlerForParsing;      // ContentHandler callbacks
  end;

procedure TExpatXMLReader.ConfigureExpatForParsing;
  begin
  ConfigureExpatHandlersForParsing;
  SetNameSpacesForParsing;          // NSReturnTriplets flag
  SetParameterEntitiesForParsing;   // relates to LexicalParameterFeature
  end;

// Precondition: Parsing = True! No check performed!
// Returns if external parameter entity parsing is *actually* enabled
function TExpatXMLReader.ExpatGetParamEntityParsing: Boolean;
  begin
  Result := FExternalParameter;
  if Result then Result := XMLSetParamEntityParsing(
    FEntityParseData.Parser, XML_PARAM_ENTITY_PARSING_ALWAYS) <> 0;
  end;

// Note: - no built-in ability to resolve, relies on EntityResolver(2)
//       - FEntityResolver must be assigned
function TExpatXMLReader.ResolveExternalEntity(Name: PSAXChar; NameLen: Integer;
  PubId, BaseURI, SysId: PSAXChar): IInputSource;
  begin
  Result := nil;
  Assert(FEntityResolver <> nil);
  if HasEntityResolver2 and
    (Name <> nil) and (Name^ <> XML_NULL) and (NameLen <> 0) then
    // sysId does not need to be absolutized, as we pass the base URI
    Result := IEntityResolver2(FEntityResolver).ResolveEntity(
      Name, PubId, BaseURI, SysId)
  else
    //kw must absolutize systemId using BaseURI
    Result := FEntityResolver.ResolveEntity(PubId, SysId);
  end;

function TExpatXMLReader.ResolveForeignDTD(Name: PSAXChar; NameLen: Integer): IInputSource;
  var
    BaseURI: PSAXChar;
  begin
  if (FEntityResolver <> nil) and HasEntityResolver2 then
    begin
    BaseURI := XMLGetBase(FEntityParseData.Parser);
    Result := IEntityResolver2(FEntityResolver).GetExternalSubset(Name, BaseURI);
    end
  else
    Result := nil;
  end;

procedure TExpatXMLReader.CleanupParseData;
  begin
  Attributes.AttrDecls.Clear;
  ParamEntityDecls.Clear;
  // move left-over TEntityParseData instances to free list
  while FEntityParseData <> nil do PopEntityParseData;
  with FParseData do
    begin
    if Locator <> nil then Locator._Release;
    Locator := nil;
    ParseException.Free;
    ParseException := nil;
    Started := False;
    Standalone := -1;
    FreeMem(NameBuf);
    NameBuf := nil;
    NameBufLen := 0;
    ContentStream.Free;
    ContentStream := nil;
    HasDocTypeDecl := False;
    ForeignDTDSource := nil;
    ContentModel := nil;
    end;
  end;

// Precondition: FEntityParseData must be set up correctly
function TExpatXMLReader.ContinueEntityParsing: TDocParseStatus;
  var
    IsFinal: LongBool;
    Buffer: Pointer;
    ReadCount: Integer;
    Status: TXML_Status;
  begin
  with FEntityParseData do
    begin
    IsFinal := False;
    while not IsFinal do with FParseData do
      begin
      Buffer := XMLGetBuffer(Parser, XML_READBUFSIZE);
      if Buffer = nil then OutOfMemoryError;
      if InStream <> nil then
        begin
        ReadCount := InStream.Read(Buffer^, XML_READBUFSIZE);
        IsFinal := ReadCount < XML_READBUFSIZE;
        end
      else
        begin
        if InReader.State <> iosClosed then
          begin
          ReadCount := InReader.Read(Buffer^, XML_READBUFSIZE);
          IsFinal := InReader.State = iosClosed;
          end
        else
          begin
          ReadCount := 0;
          IsFinal := True;
          end;
        end;
      Status := XMLParseBuffer(Parser, ReadCount, Integer(IsFinal));
      case Status of
        XML_STATUS_OK:
          Continue;
        XML_STATUS_ERROR:
          begin
          if XMLGetErrorCode(Parser) = XML_ERROR_ABORTED then
            begin
            Result := dpsAborted;
            Exit;
            end;
          if ProcessExpatError(Parser) = setFatal then
            begin
            Result := dpsFatalError;
            Exit;
            end;
          // if the error is not fatal we can continue
          Continue;
          end;
{$IFDEF EXPAT_1_95_8_UP}
        XML_STATUS_SUSPENDED:
          begin
          Result := dpsSuspended;
          Exit;
          end;
{$ENDIF}
        else
          raise ESAXExpat.Create(SUnexpectedParserResult);
        end;
      end;
    end;
  Result := dpsFinished;
  end;

function TExpatXMLReader.StartParsing(const Source: IInputSource;
  IsStreamSource: Boolean): TDocParseStatus;
  var
    Parser: TXMLParser;
  begin
  if FEntityParseData <> nil then CleanupParseData;
  // set up context record for document entity
  PushDocumentEntityParseData(Source, IsStreamSource);
  Parser := FEntityParseData.Parser;

  FParseData.Locator := TExpatLocator.Create(Self);
  FParseData.Locator._AddRef;  // make sure reference counting is OK!

  if (FEntityResolver <> nil) and FHasEntityResolver2 then
    XMLUseForeignDTD(Parser, True);
  XMLSetXMLDeclHandler(Parser, @XMLDeclHandler);
  // Need the next two handlers because storing parameter entity
  // declarations requires them, and because the ContentHandler or
  // LexicalHandler could be turned on at runtime.
  XMLSetEntityDeclHandler(Parser, @EntityDeclHandler);
  // needs to be set all the time, to report skipped external entities
  XMLSetExternalEntityRefHandler(Parser, @ExternalEntityRefHandler);
  XMLSetDocTypeDeclHandler(Parser, @StartDocTypeDeclHandler, @EndDocTypeDeclHandler);
  XMLSetNotStandaloneHandler(Parser, @NotStandaloneHandler);

  ConfigureExpatForParsing;

  if FContentHandler <> nil then
    begin
    FContentHandler.SetDocumentLocator(IExpatLocator(FParseData.Locator));
    FContentHandler.StartDocument;
    end;
  FParseData.Started := True;
  Result := ContinueEntityParsing;
  end;

procedure TExpatXMLReader.FinishParsing;
  begin
  if FParseData.Started and (FContentHandler <> nil) then
    FContentHandler.EndDocument;
  end;

{$IFDEF EXPAT_1_95_8_UP}
// requires FEntityParseData <> nil
function TExpatXMLReader.ResumeParsing: TDocParseStatus;
  var
    ParsingStatus: TXMLParsingStatus;
    Status: TXML_Status;
  begin
  with FEntityParseData do
    begin
    Status := XMLResumeParser(Parser);
    case Status of
      XML_STATUS_OK:
        begin
        XMLGetParsingStatus(Parser, ParsingStatus);
        if ParsingStatus.FinalBuffer then
          begin
          Result := dpsFinished;
          Exit;
          end;
        end;
      XML_STATUS_ERROR:
        begin
        if XMLGetErrorCode(Parser) = XML_ERROR_ABORTED then
          begin
          Result := dpsAborted;
          Exit;
          end;
        if ProcessExpatError(Parser) = setFatal then
          begin
          Result := dpsFatalError;
          Exit;
          end;
        // if the error was not fatal we can continue
        end;
      XML_STATUS_SUSPENDED:
        begin
        Result := dpsSuspended;
        Exit;
        end;
      else
        raise ESAXExpat.Create(SUnexpectedParserResult);
      end;
    end;
  Result := ContinueEntityParsing;
  end;
{$ENDIF}

{ Prerequisite: FEntityParseData <> nil, FEntityParseData.Parent <> nil
  Postcondition: PopEntityParseData was called unless there was an exception }
procedure TExpatXMLReader.FinishExternalEntityParsing;
  begin
  with FEntityParseData do
    begin
    Assert(Parent <> nil);
    if FLexicalHandler <> nil then
      begin
      FLexicalHandler.EndEntity(Name, NameLen);
      // a foreign DTD without DocType declaration would not trigger a
      // EndDocTypeDecl call-back from Expat, so we call EndDTD here
      with FParseData do
        if (ForeignDTDSource <> nil) and not HasDocTypeDecl then
          FLexicalHandler.EndDTD;
      end;
    end;
  // restore parent entity context
  PopEntityParseData;
  with FParseData do
    if ForeignDTDSource <> nil then ForeignDTDSource := nil;
  { In expat, a child parser (for parsing external references) will
    inherit all context and handlers from the parent, but will not
    give changes back to the parent parser;
    this is a problem when handlers or configuration items have changed;
    therefore, we have to track those changes and update the configuration
    of the parent parser when returning from parsing an external entity }
  ConfigureExpatHandlersForParsing;  // FEntityParseData <> nil
  end;
  
function TExpatXMLReader.GetContentHandler: IBufferedContentHandler;
  begin
  Result := FContentHandler;
  end;

function TExpatXMLReader.GetDeclHandler: IBufferedDeclHandler;
  begin
  Result := FDeclHandler;
  end;

function TExpatXMLReader.GetDTDHandler: IBufferedDTDHandler;
  begin
  Result := FDTDHandler;
  end;

function TExpatXMLReader.GetEntityResolver: IEntityResolver;
  begin
  Result := FEntityResolver;
  end;

function TExpatXMLReader.GetErrorHandler: IErrorHandler;
  begin
  Result := FErrorHandler;
  end;

function TExpatXMLReader.GetHasEntityResolver2: Boolean;
  begin
  Result := FHasEntityResolver2;
  end;

function TExpatXMLReader.GetLexicalHandler: IBufferedLexicalHandler;
  begin
  Result := FLexicalHandler;
  end;

function TExpatXMLReader.GetParseUnlessStandalone: Boolean;
  begin
  Result := FParseUnlessSTandalone;
  end;

function TExpatXMLReader.GetSkipInternal: Boolean;
  begin
  Result := FSkipInternal;
  end;

function TExpatXMLReader.GetStandaloneError: Boolean;
  begin
  Result := FStandaloneError;
  end;

class function ExpatFeatures: TExpatFeaturesClass;
  begin
  Result := TExpatHashedFeatures;
  end;

function TExpatXMLReader.GetFeature(const Name: SAXString): Boolean;
  begin
  Result := ExpatFeatures.GetFeature(Self, Name);
  end;

procedure TExpatXMLReader.SetFeature(const Name: SAXString; Value: Boolean);
  begin
  ExpatFeatures.SetFeature(Self, Name, Value);
  end;

function TExpatXMLReader.GetProperty(const Name: SAXString): IProperty;
  begin
  if Name = LexicalHandlerProperty then
    Result := IExpatLexicalHandlerProperty(TExpatCoreProperties(FProperties))
  else if Name = DeclHandlerProperty then
    Result := IExpatDeclHandlerProperty(TExpatCoreProperties(FProperties))
  else if Name = DefaultHandlerProperty then
    Result := IExpatDefaultHandlerProperty(TExpatCoreProperties(FProperties))
  else
    Result := nil;
    // why should we raise an exception here?  //kw
    // it should be enough to return nil for "not recognized"
    {raise ESAXNotRecognizedException.CreateFmt(
      SPropertyNotRecognized, [SAXString(Name)]);}
  end;

function TExpatXMLReader.GetStandalone: TXMLDocStandalone;
  begin
  if Parsing then
    begin
    if FParseData.Standalone < 0 then Result := xdsUndefined
    else if FParseData.Standalone > 0 then Result := xdsYes
    else Result := xdsNo;
    end
  else
    raise ESAXNotSupportedException.CreateFmt(
      SFeatureWhenParsing, [IsStandaloneFeature])
  end;

function TExpatXMLReader.IsStandalone: Boolean;
  begin
  Result := GetStandalone = xdsYes;
  end;

procedure TExpatXMLReader.ParseSource(const Source: IInputSource;
  IsStreamSource: Boolean);
  var
    Status: TDocParseStatus;
  begin
  try
    Status := StartParsing(Source, IsStreamSource);
    if Status <> dpsSuspended then
      begin
      FinishParsing;
      CleanupParseData;
      end;
  except
    CleanupParseData;
    raise;
    end;
  end;

{ Currently only streams and files are supported }
procedure TExpatXMLReader.Parse(const Input: IInputSource);
  var
    StreamSource: IStreamInputSource;
    ReaderSource: IReaderInputSource;
  begin
  if Input = nil then raise ESAXExpat.Create(SInvalidInputSource);
  if Input.QueryInterface(IStreamInputSource, StreamSource) = S_OK then
    ParseSource(StreamSource, True)
  else if Input.QueryInterface(IReaderInputSource, ReaderSource) = S_OK then
    ParseSource(ReaderSource, False)
  else
    // we can deal with IStreamInputSource and IReaderInputSource only,
    raise ESAXExpat.Create(SStreamReaderSourceOnly);
  end;

{ Currently only files are supported - SystemId *MUST* be an absolute URI }
procedure TExpatXMLReader.Parse(const SystemId: SAXString);
  var
    InSource: IInputSource;
  begin
  //kw systemId must be absolute URI - we should check that
  if FEntityResolver <> nil then
    InSource := ResolveExternalEntity(nil, 0, nil, nil, PSAXChar(SystemId));
  if InSource = nil then
    raise ESAXExpat.CreateFmt(SCannotResolveEntity, ['', SystemId]);
  Parse(InSource);
  end;

function TExpatXMLReader.Parsing: Boolean;
  begin
  Result := FEntityParseData <> nil;
  end;

procedure TExpatXMLReader.SetContentHandlerForParsing;
  begin
  with FEntityParseData do
    begin
    if FContentHandler <> nil then
      begin
      XMLSetElementHandler(Parser, @StartElementHandler, @EndElementHandler);
      XMLSetCharacterDataHandler(Parser, @CharacterDataHandler);
      XMLSetNameSpaceDeclHandler(Parser, @StartNamespaceDeclhandler,
        @EndNamespaceDeclhandler);
      XMLSetProcessingInstructionHandler(Parser, @ProcessingInstructionHandler);
      XMLSetSkippedEntityHandler(Parser, @SkippedEntityHandler);
      end
    else
      begin
      XMLSetElementHandler(Parser, nil, nil);
      XMLSetCharacterDataHandler(Parser, nil);
      XMLSetNameSpaceDeclHandler(Parser, nil, nil);
      XMLSetProcessingInstructionHandler(Parser, nil);
      XMLSetSkippedEntityHandler(Parser, nil);
      end;
    end;
  end;

procedure TExpatXMLReader.SetContentHandler(const Handler: IBufferedContentHandler);
  begin
  FContentHandler := Handler;
  if Parsing then SetContentHandlerForParsing;
  end;

procedure TExpatXMLReader.SetSkipInternal(Value: Boolean);
  begin
  FSkipInternal := Value;
  if Parsing then SetDefaultDataHandlerForParsing;
  end;

procedure TExpatXMLReader.SetDeclHandlerForParsing;
  begin
  with FEntityParseData do
    begin
    Assert(Parser <> nil);
    if FDeclHandler <> nil then
      begin
      XMLSetElementDeclHandler(Parser, @ElementDeclHandler);
      XMLSetAttListDeclHandler(Parser, @AttListDeclHandler);
      // XMLSetEntityDeclHandler(Parser, @EntityDeclHandler);  // always on
      end
    else
      begin
      XMLSetElementDeclHandler(Parser, nil);
      XMLSetAttListDeclHandler(Parser, nil);
      // if FDTDHandler = nil then  // clear only if nobody else needs it
      //   XMLSetEntityDeclHandler(Parser, nil);
      end;
    end;
  end;

procedure TExpatXMLReader.SetDeclHandler(const Handler: IBufferedDeclHandler);
  begin
  FDeclHandler := Handler;
  if Parsing then SetDeclHandlerForParsing;
  end;

procedure TExpatXMLReader.SetDTDHandlerForParsing;
  begin
  with FEntityParseData do
    begin
    Assert(Parser <> nil);
    if FDTDHandler <> nil then
      begin
      XMLSetNotationDeclHandler(Parser, @NotationDeclHandler);
      // XMLSetEntityDeclHandler(Parser, @EntityDeclHandler); // always on
      // UnparsedEntityDeclHandler is obsolete - use EntityDeclhandler
      // XMLSetUnparsedEntityDeclHandler(Parser, @UnparsedEntityDeclHandler);
      end
    else
      begin
      XMLSetNotationDeclHandler(Parser, nil);
      // if FDeclHandler = nil then  // clear only if no one else needs it
      //   XMLSetEntityDeclHandler(Parser, nil);
      end;
    end;
  end;

procedure TExpatXMLReader.SetDTDHandler(const Handler: IBufferedDTDHandler);
  begin
  FDTDHandler := Handler;
  if Parsing then SetDTDHandlerForParsing;
  end;

procedure TExpatXMLReader.SetEntityResolver(const Resolver: IEntityResolver);
  begin
  if (Resolver <> nil) and (Resolver.QueryInterface(
    IEntityResolver2, FEntityResolver) = 0) then
    begin
    FHasEntityResolver2 := True;
    end
  else
    begin
    FEntityResolver := Resolver;
    FHasEntityResolver2 := False;
    end;
  end;

procedure TExpatXMLReader.SetErrorHandler(const Handler: IErrorHandler);
  begin
  FErrorHandler := Handler;
  end;

procedure TExpatXMLReader.SetExternalGeneral(Value: Boolean);
  begin
  FExternalGeneral := Value;
  end;

procedure TExpatXMLReader.SetExternalParameter(Value: Boolean);
  begin
  FExternalParameter := Value;
  end;

function TExpatXMLReader.GetParameterEntities: Boolean;
  begin
  Result := FParameterEntities;
  end;

{$IFNDEF DELPHI6_UP}
function BoolToStr(B: Boolean; UseBoolStrs: Boolean = False): string;
  const
    SimpleBoolStrs: array [Boolean] of string = ('0', '-1');
    BoolStrs: array [Boolean] of string = ('False', 'True');
  begin
  if UseBoolStrs then
    Result := BoolStrs[B]
  else
    Result := SimpleBoolStrs[B];
  end;
{$ENDIF}

procedure TExpatXMLReader.SetParameterEntitiesForParsing;
  var
    Error: Exception;
    ParEntParsing: TXMLParamEntityParsing;
  begin
  with FEntityParseData do
    begin
    Assert(Parser <> nil);
    if FParameterEntities then
      begin
      if ParseUnlessStandalone then
        ParEntParsing := XML_PARAM_ENTITY_PARSING_UNLESS_STANDALONE
      else
        ParEntParsing := XML_PARAM_ENTITY_PARSING_ALWAYS;
      end
    else
      ParEntParsing := XML_PARAM_ENTITY_PARSING_NEVER;
    if XMLSetParamEntityParsing(Parser, ParEntParsing) = 0 then
      begin
      Error := ESAXNotSupportedException.CreateFmt(SFeatureNotSupported,
        [LexicalParameterFeature, BoolToStr(FParameterEntities, True)]);
      try
        CallErrorHandler(setError, Error);
      finally
        Error.Free;
        end;
      end;
    end;
  end;

procedure TExpatXMLReader.SetParameterEntities(Value: Boolean);
  begin
  FParameterEntities := Value;
  if Parsing then SetParameterEntitiesForParsing;
  end;

// If ParseUnlessStandalone = True, then external parameter entities will
// not be parsed if the XML declaration specifies Standalone = Yes;
// it also means that the NotStandaloneHandler will not be called in this case
procedure TExpatXMLReader.SetParseUnlessStandalone(Value: Boolean);
  begin
  FParseUnlessStandalone := Value;
  if Parsing then SetParameterEntitiesForParsing;
  end;

procedure TExpatXMLReader.SetLexicalHandlerForParsing;
  begin
  with FEntityParseData do
    begin
    Assert(Parser <> nil);
    if FLexicalHandler <> nil then
      begin
      XMLSetCommentHandler(Parser, @CommentHandler);
      XMLSetCdataSectionHandler(Parser, @StartCdataSectionHandler,
         @EndCdataSectionHandler);
      // XMLSetDocTypeDeclHandler(Parser, @StartDocTypeDeclHandler, // always on
      //   @EndDocTypeDeclHandler);       // needed to store the DTD declaration
      end
    else
      begin
      XMLSetCommentHandler(Parser, nil);
      XMLSetCdataSectionHandler(Parser, nil, nil);
      // XMLSetDocTypeDeclHandler(Parser, nil, nil);
      end;
    end;
  end;

procedure TExpatXMLReader.SetLexicalHandler(const Handler: IBufferedLexicalHandler);
  begin
  FLexicalHandler := Handler;
  if Parsing then SetLexicalHandlerForParsing;
  end;

procedure TExpatXMLReader.SetNamespacePrefixes(Value: Boolean);
  begin
  if Value then raise ESAXNotSupportedException.CreateFmt(
    SFeatureNotSupported, [NamespacePrefixesFeature, BoolToStr(Value, True)])
  else
    FNamespacePrefixes := Value;
  end;

// when parsing, this will only affect the reporting of QNames, and only
// if FNamespaces was True at the start of parsing
procedure TExpatXMLReader.SetNamespaces(Value: Boolean);
  begin
  FNamespaces := Value;
  if Parsing then SetNamespacesForParsing;
  end;

procedure TExpatXMLReader.SetNameSpacesForParsing;
  begin
  with FEntityParseData do
    begin
    Assert(Parser <> nil);
    XMLSetReturnNSTriplet(Parser, Ord(FNameSpaces));
    end;
  end;

procedure TExpatXMLReader.SetStandaloneError(Value: Boolean);
  begin
  FStandaloneError := Value;
  end;

procedure TExpatXMLReader.SetValidation(Value: Boolean);
  begin
  if Value then raise ESAXNotSupportedException.CreateFmt(
    SFeatureNotSupported, [ValidationFeature, BoolToStr(Value, True)])
  else
    FValidation := Value;
  end;

function TExpatXMLReader.GetExternalGeneral: Boolean;
  begin
  Result := FExternalGeneral;
  end;

function TExpatXMLReader.GetExternalParameter: Boolean;
  begin
  Result := FExternalParameter;
  end;

function TExpatXMLReader.GetNamespacePrefixes: Boolean;
  begin
  Result := FNamespacePrefixes;
  end;

function TExpatXMLReader.GetNamespaces: Boolean;
  begin
  Result := FNamespaces;
  end;

function TExpatXMLReader.GetValidation: Boolean;
  begin
  Result := FValidation;
  end;

function TExpatXMLReader.GetUseAttributes2: Boolean;
  var
    // use interface type for Dummy, not a plain Pointer, because
    // QueryInterface calls _AddRef, so we need _Release called too
    Dummy: IBufferedAttributes2;
  begin
  Result :=
    IUnknown(FAttributes).QueryInterface(IBufferedAttributes2, Dummy) = 0;
  end;

function TExpatXMLReader.GetUseLocator2: Boolean;
  begin
  Result :=  True;  // not dynamic - make sure this is updated when necessary
  end;

function TExpatXMLReader.GetUseEntityResolver2: Boolean;
  begin
  Result :=  True;  // not dynamic - make sure this is updated when necessary
  end;

function TExpatXMLReader.GetDefaultDataHandler: IBufferedDefaultHandler;
  begin
  Result := FDefaultDataHandler;
  end;

{ Note: There are two choices, XMLSetDefaultHandlerExpand and
        XMLSetDefaultHandler. The latter will skip internal entities,
        the former will expand them silently                         }
procedure TExpatXMLReader.SetDefaultDataHandlerForParsing;
  begin
  with FEntityParseData do
    begin
    Assert(Parser <> nil);
    if FDefaultDataHandler <> nil then
      begin
      if FSkipInternal then // all unhandled data, skip internal entities
        XMLSetDefaultHandler(Parser, @DefaultXMLHandler)
      else                  // all unhandled data, expand internal entities
        XMLSetDefaultHandlerExpand(Parser, @DefaultXMLHandler);
      end
    else
      begin
      if FSkipInternal then // skip internal entities
        XMLSetDefaultHandler(Parser, nil)
      else                  // expand internal entities
        XMLSetDefaultHandlerExpand(Parser, nil);
      end;
    end;
  end;

procedure TExpatXMLReader.SetDefaultDataHandler(const Handler: IBufferedDefaultHandler);
  begin
  FDefaultDataHandler := Handler;
  if Parsing then SetDefaultDataHandlerForParsing;
  end;

function TExpatXMLReader.CreateAttributes: TExpatAttributes;
  begin
  Result := TExpatAttributes.Create(Self);
  end;

function TExpatXMLReader.CreateProperties: TExpatProperties;
  begin
  Result := TExpatCoreProperties.Create(Self);
  end;

class function TExpatXMLReader.ExpatFeatures: TExpatFeaturesClass;
  begin
  Result := TExpatHashedFeatures;
  end;

function TExpatXMLReader.GetBaseURI: PSAXChar;
  begin
  if Parsing then Result := XMLGetBase(FEntityParseData.Parser)
  else raise ESAXExpat.Create(SIllegalWhenNotParsing);
  end;

procedure TExpatXMLReader.SetBaseURI(Value: PSAXChar);
  begin
  if Parsing then
    begin
    //kw we should check that Value is an absolute URI
    if XMLSetBase(FEntityParseData.Parser, Value) = XML_STATUS_ERROR then
      OutOfMemoryError;
    end
  else
    raise ESAXExpat.Create(SIllegalWhenNotParsing);
  end;

function TExpatXMLReader.GetParser: TXMLParser;
  begin
  Result := FEntityParseData.Parser;
  end;

function TExpatXMLReader.GetExpatVersion: SAXString;
  begin
  Result := XMLExpatVersion;
  end;

function TExpatXMLReader.GetForeignDoctypeName: SAXString;
  begin
  Result := FForeignDoctypeName;
  end;

procedure TExpatXMLReader.SetForeignDoctypeName(const Value: SAXString);
  begin
  FForeignDoctypeName := Value;
  end;

function TExpatXMLReader.GetResolveDTDURIs: Boolean;
  begin
  Result := False;
  end;

{$IFDEF EXPAT_1_95_8_UP}
procedure TExpatXMLReader.Abort;
  var
    ErrCode: TXMLError;
  begin
  if FEntityParseData = nil then
    raise ESAXExpat.Create(SIllegalWhenNotParsing);
  with FEntityParseData do
    begin
    Assert(Parser <> nil);
    // no cleanup here, as we are in the middle of a call-back
    if XMLStopParser(Parser, False) = XML_STATUS_OK then Exit;
    ErrCode := XMLGetErrorCode(Parser);
    // if we are at end of entity, ignore error
    if ErrCode = XML_ERROR_FINISHED then Exit;
    raise ESAXExpat.Create(XMLErrorString(ErrCode));
    end;
  end;

function TExpatXMLReader.GetStatus: TXMLReaderStatus;
  var
    ParsingStatus: TXMLParsingStatus;
  begin
  if FEntityParseData = nil then
    Result := xrsReady
  else with FEntityParseData do
    begin
    Assert(FEntityParseData.Parser <> nil);
    XMLGetParsingStatus(FEntityParseData.Parser, ParsingStatus);
    case ParsingStatus.Parsing of
      XML_INITIALIZED: Result := xrsReady;
      XML_PARSING: Result := xrsParsing;
      XML_FINISHED: Result := xrsParsing;
      XML_SUSPENDED: Result := xrsSuspended;
      else
        Result := xrsParsing;
      end;
    end;
  end;

procedure TExpatXMLReader.Resume;
  var
    Status: TDocParseStatus;
    ParsingDone: Boolean;
  begin
  if FEntityParseData = nil then
    raise ESAXExpat.Create(SIllegalWhenNotSuspended);
  try
    ParsingDone := False;
    repeat
      Status := ResumeParsing;
      case Status of
        dpsFinished:
          begin
          if FEntityParseData.Parent = nil then
            begin
            FinishParsing;
            CleanupParseData;
            ParsingDone := True;
            end
          else
            // return to parsing parent entity
            FinishExternalEntityParsing;
          end;
        dpsSuspended:
          ParsingDone := True;
        dpsFatalError:
          begin
          FinishParsing;
          CleanupParseData;
          ParsingDone := True;
          end;
        dpsAborted:
          begin
          CleanupParseData;
          ParsingDone := True;
          end;
        else  // should not happen
          ParsingDone := True;
        end;
      until ParsingDone;
  except
    CleanupParseData;
    raise;
    end;
  end;

procedure TExpatXMLReader.Suspend;
  var
    ErrCode: TXMLError;
  begin
  if FEntityParseData = nil then
    raise ESAXExpat.Create(SIllegalWhenNotParsing);
  with FEntityParseData do
    begin
    Assert(Parser <> nil);
    if XMLStopParser(Parser, True) <> XML_STATUS_OK then
      begin
      ErrCode := XMLGetErrorCode(Parser);
      case ErrCode of
        XML_ERROR_FINISHED,    // parser finished - ignore error
        XML_ERROR_SUSPEND_PE:  // parser not suspendable - ignore error
          Exit;
        end;
      raise ESAXExpat.Create(XMLErrorString(ErrCode));
      end;
    end;
  end;
{$ENDIF}

procedure TExpatXMLReader.DisposeEntityParsedata(EPDList: TEntityParseData);
  var
    ParentPD: TEntityParseData;
  begin
  while EPDList <> nil do
    begin
    ParentPD := EPDList.Parent;
    EPDList.Free;
    EPDList := ParentPD;
    end;
  end;

procedure TExpatXMLReader.PushEntityParseData;
  var
    ParentEntityParseData: TEntityParseData;
  begin
  ParentEntityParseData := FEntityParseData;
  if FFreeEntityParseData <> nil then  // use instance from free list
    begin
    FEntityParseData := FFreeEntityParseData;
    FFreeEntityParseData := FFreeEntityParseData.Parent;
    end
  else
    begin
    FEntityParseData := TEntityParseData.Create;
    end;
  FEntityParseData.FParent := ParentEntityParseData;
  end;

procedure TExpatXMLReader.PushDocumentEntityParseData(const Source: IInputSource;
  IsStreamSource: Boolean);
  var
    NSSep: TXMLCHAR;
    Parser: TXMLParser;
    Enc: WideString;
    EncodingP: PXMLChar;
  begin
  PushEntityParseData;
  try
    NSSep := TXMLCHAR(XML_NSSEP);
    Enc := Source.Encoding;
    if Enc = '' then EncodingP := nil else EncodingP := PXMLChar(Enc);
    if Namespaces then
      Parser := XMLParserCreateNS(EncodingP, NSSep)
    else
      Parser := XMLParserCreate(EncodingP);
    if Parser = nil then OutOfMemoryError;
    XMLSetUserData(Parser, Self);
    FEntityParseData.Init(Source, IsStreamSource, Parser);
    // set base URI after initializing FEntityParseData so that we have
    // access to the SystemId without a new string copy from Source
    if XMLSetBase(Parser, PSAXChar(FEntityParseData.SystemId))
                          = XML_STATUS_ERROR then OutOfMemoryError;
  except
    PopEntityParseData;
    raise;
    end;
  end;

function TExpatXMLReader.PushChildEntityParseData(ParentParser: TXMLParser;
  Name: PSAXChar; NameLen: Integer; Context: PSAXChar;
  const Source: IInputSource; IsStreamSource: Boolean): Boolean;
  var
    Parser: TXMLParser;
    Enc: WideString;
    EncodingP: PXMLChar;
  begin
  Result := False;
  PushEntityParseData;
  try
    Enc := Source.Encoding;
    if Enc = '' then EncodingP := nil else EncodingP := PXMLChar(Enc);
    Parser := XMLExternalEntityParserCreate(ParentParser, Context, EncodingP);
    if Parser = nil then OutOfMemoryError;
    FEntityParseData.Init(Source, IsStreamSource, Parser, Name, NameLen);
    // set base URI after initializing FEntityParseData so that we have
    // access to the SystemId without a new string copy from Source
    if XMLSetBase(Parser, PSAXChar(FEntityParseData.SystemId))
                          = XML_STATUS_ERROR then OutOfMemoryError;
    Result := True;
  except
    on E: Exception do
      begin
      PopEntityParseData;
      CallErrorHandler(setError, E.Message);
      end
    else
      PopEntityParseData;
      CallErrorHandler(setError, SInitParseError);
    end;
  end;

procedure TExpatXMLReader.PopEntityParseData;
  var
    ParentEntityParseData: TEntityParseData;
    Parser: TXMLParser;
  begin
  ParentEntityParseData := FFreeEntityParseData;
  FFreeEntityParseData := FEntityParseData;
  FEntityParseData := FEntityParseData.Parent;
  FFreeEntityParseData.FParent := ParentEntityParseData;
  // clean up "popped" instance
  Parser := FFreeEntityParseData.Parser;
  if Parser <> nil then XMLParserFree(Parser);
  FFreeEntityParseData.Clear;
  end;

// requires FEntityParseData <> nil
function TExpatXMLReader.ProcessExpatError(Parser: TXMLParser): TSAXErrorType;
  var
    ErrorStr: string;
    ErrorCode: TXMLError;
  begin
  ErrorCode := XMLGetErrorCode(Parser);
  if ErrorCode in OperationalErrors then
    Result := setError
  else  // well-formedness errors are always fatal
    Result := setFatal;
  ErrorStr := XMLErrorString(ErrorCode);
  with FParseData do
    begin
    if ParseException = nil then
      ParseException := ESAXExpatParse.Create(ErrorStr, Locator, Integer(ErrorCode))
    else
      ParseException.Init(ErrorStr, Locator, Integer(ErrorCode));
    CallNativeErrorHandler(Result, ParseException);
    end;
  end;

function TExpatXMLReader.GetReaderControl: Boolean;
 begin
{$IFDEF EXPAT_1_95_8_UP}
  Result := True;
{$ELSE}
  Result := False;
{$ENDIF}
 end;

function TExpatXMLReader.GetContentModel: PXMLContent;
  begin
  Result := FParseData.ContentModel;
  end;

{ TExpatAttributes }

constructor TExpatAttributes.Create(Reader: TExpatXMLReader);
  begin
  inherited Create(Reader);
  FAttrDecls := TSAXAttributeDecls.Create;
  end;

destructor TExpatAttributes.Destroy;
  begin
  FreeMem(FQNameBuf);
  FAttrDecls.Free;
  inherited Destroy;
  end;

function TExpatAttributes.GetIndex(Uri: PSAXChar; UriLen: Integer;
  LocalName: PSAXChar; LocalNameLen: Integer): Integer;
  var
    Name: PXMLChar;
    AttUri, AttLocal: PSAXChar;
    AttUriLen, AttLocalLen: Integer;
  begin
  Result := FTotalCount - 1;
  while Result >= 0 do
    begin
    Name := FAtts^[Result shl 1];
    SAXParseNameUriLocal(Name, AttUri, AttUriLen, AttLocal, AttLocalLen);
    if (UriLen = AttUriLen) and (LocalNameLen = AttLocalLen) and
      SAXStrEqual(Uri, AttUri, UriLen) and
      SAXStrEqual(LocalName, AttLocal, LocalNameLen) then Break;
    Dec(Result);
    end;
  end;

function TExpatAttributes.GetIndex(QName: PSAXChar; QNameLen: Integer): Integer;
  var
    Name: PXMLChar;
    AttQName: PSAXChar;
    AttQNameLen: Integer;
  begin
  Result := FTotalCount - 1;
  while Result >= 0 do
    begin
    Name := FAtts^[Result shl 1];
    SAXParseNameQual(Name, AttQName, AttQNameLen, FQNameBuf, FQNameBufLen);
    if (QNameLen = AttQNameLen) and
      SAXStrEqual(QName, AttQName, QNameLen) then Break;
    Dec(Result);
    end;
  end;

function TExpatAttributes.GetLength: Integer;
  begin
  Result := FTotalCount;
  end;

procedure TExpatAttributes.GetLocalName(Index: Integer; out LocalName: PSAXChar;
  out LocalNameLen: Integer);
  var
    Name: PXMLChar;
  begin
  CheckIndex(Index);
  Name := FAtts^[Index shl 1];
  SaxParseNameLocal(Name, LocalName, LocalNameLen);
  end;

{ not part of SAX2 definition, but more efficient as the individual calls }
procedure TExpatAttributes.GetName(Index: Integer; out Uri: PSAXChar;
  out UriLen: Integer; out LocalName: PSAXChar; out LocalNameLen: Integer;
  out QName: PSAXChar; out QNameLen: Integer);
  var
    Name: PXMLChar;
  begin
  CheckIndex(Index);
  Name := FAtts^[Index shl 1];
  SaxParseName(Name, Uri, UriLen, LocalName, LocalNameLen, QName, QNameLen,
    FQNameBuf, FQNameBufLen);
  end;

procedure TExpatAttributes.GetQName(Index: Integer; out QName: PSAXChar;
  out QNameLen: Integer);
  var
    Name: PXMLChar;
  begin
  CheckIndex(Index);
  Name := FAtts^[Index shl 1];
  SaxParseNameQual(Name, QName, QNameLen, FQNameBuf, FQNameBufLen);
  end;

{ DTDs don't know about namespaces, so to find the attribute in the list
  of declarations, the QName (literal name) must match                   }
procedure TExpatAttributes.GetType(Index: Integer; out AType: PSAXChar;
  out ATypeLen: Integer);
  var
    QName: PSAXChar;
    QNameLen: Integer;
    AttrType: SAXString;
    ParChr: PSAXChar;
  begin
  // FindDecl is based on matching QNames, i.e. the declarations in
  // the DTD must have the same QNames as the element and attribute
  // names encountered in the document - matching URIs is not sufficient
  GetQName(Index, QName, QNameLen);
  if AttrDecls.FindDecl(FElement, FElementLen, QName, QNameLen) then
    begin
    // see specs on http://sax.sourceforge.net, the Expat docs, and
    // the XML 1.0 specs, chapter 3.3.1, Attribute Types at
    // http://www.w3.org/TR/2000/REC-xml-20001006
    AttrType := AttrDecls.GetType;
    AType := PSAXChar(AttrType);
    // check for enumerated type or notation - must have left parenthesis
    ParChr := SAXStrScan(AType, XML_LEFTPAR); // we assume whitespace was removed
    if ParChr = nil then
      // not found - regular type
      begin
      // AType := PSAXChar(AttrType);  // already set
      ATypeLen := Length(AttrType);
      end
    else if ParChr = AType then
      // found at first position: we have an enumerated type
      begin
      // SAX spec says: report as NMTOKEN
      { AType := PSAXChar(XML_NMTOKEN);
        ATypeLen := Length(XML_NMTOKEN); }
      // XML Infoset spec says: report as ENUMERATION
      AType := PSAXChar(XML_ENUMERATION);
      ATypeLen := Length(XML_ENUMERATION);
      end
    else
      // we have a notation, since left parenthesis is not first character
      begin
      AType := PSAXChar(XML_NOTATION);
      ATypeLen := Length(XML_NOTATION);
      end;
    end
  else
    begin
    AType := XML_CDATA; // default
    ATypeLen := Length(XML_CDATA);
    end;
  end;

procedure TExpatAttributes.GetType(Uri: PSAXChar; UriLen: Integer;
  LocalName: PSAXChar; LocalNameLen: Integer; out AType: PSAXChar;
  out ATypeLen: Integer);
  var
    Indx: Integer;
  begin
  Indx := GetIndex(Uri, UriLen, LocalName, LocalNameLen);
  if Indx >= 0 then GetType(Indx, AType, ATypeLen)
  else NotFoundError(Uri, UriLen, LocalName, LocalNameLen);
  end;

procedure TExpatAttributes.GetType(QName: PSAXChar; QNameLen: Integer;
  out AType: PSAXChar; out ATypeLen: Integer);
  var
    Indx: Integer;
  begin
  Indx := GetIndex(QName, QNameLen);
  if Indx >= 0 then GetType(Indx, AType, ATypeLen)
  else NotFoundError(QName, QNameLen);
  end;

procedure TExpatAttributes.GetURI(Index: Integer; out Uri: PSAXChar;
  out UriLen: Integer);
  var
    Name: PXMLChar;
  begin
  CheckIndex(Index);
  Name := FAtts^[Index shl 1];
  SaxParseNameUri(Name, Uri, UriLen);
  end;

procedure TExpatAttributes.GetValue(Index: Integer; out Value: PSAXChar;
  out ValueLen: Integer);
  begin
  CheckIndex(Index);
  Value := FAtts^[(Index shl 1) + 1];
  if LongBool(Value) then ValueLen := -1 else ValueLen := 0;
  end;

procedure TExpatAttributes.GetValue(Uri: PSAXChar; UriLen: Integer;
  LocalName: PSAXChar; LocalNameLen: Integer; out Value: PSAXChar;
  out ValueLen: Integer);
  var
    Indx: Integer;
  begin
  Indx := GetIndex(Uri, UriLen, LocalName, LocalNameLen);
  if Indx >= 0 then GetValue(Indx, Value, ValueLen)
  else NotFoundError(Uri, UriLen, LocalName, LocalNameLen);
  end;

procedure TExpatAttributes.GetValue(QName: PSAXChar;
  QNameLen: Integer; out Value: PSAXChar; out ValueLen: Integer);
  var
    Indx: Integer;
  begin
  Indx := GetIndex(QName, QNameLen);
  if Indx >= 0 then GetValue(Indx, Value, ValueLen)
  else NotFoundError(QName, QNameLen);
  end;

function TExpatAttributes.IsDeclared(Index: Integer): Boolean;
  var
    QName: PSAXChar;
    QNameLen: Integer;
  begin
  // FindDecl is based on matching QNames, i.e. the declarations in
  // the DTD must have the same QNames as the element and attribute
  // names encountered in the document - matching URIs is not sufficient
  GetQName(Index, QName, QNameLen);
  Result := AttrDecls.FindDecl(FElement, FElementLen, QName, QNameLen);
  end;

function TExpatAttributes.IsDeclared(Uri: PSAXChar; UriLen: Integer;
  LocalName: PSAXChar; LocalNameLen: Integer): Boolean;
  var
    Indx: Integer;
  begin
  Indx := GetIndex(Uri, UriLen, LocalName, LocalNameLen);
  if Indx < 0 then NotFoundError(Uri, UriLen, LocalName, LocalNameLen);
  Result := IsDeclared(Indx);
  end;

function TExpatAttributes.IsDeclared(QName: PSAXChar; QNameLen: Integer): Boolean;
  var
    Indx: Integer;
  begin
  Indx := GetIndex(QName, QNameLen);
  if Indx < 0 then NotFoundError(QName, QNameLen);
  Result := IsDeclared(Indx);
  end;

function TExpatAttributes.IsSpecified(Index: Integer): Boolean;
  begin
  CheckIndex(Index);
  Result := Index < SpecCount;
  end;

{$WARNINGS OFF}  // don't warn about unitialized variables
function TExpatAttributes.IsSpecified(QName: PSAXChar; QNameLen: Integer): Boolean;
  var
    Indx: Integer;
  begin
  Indx := GetIndex(QName, QNameLen);
  if Indx >= 0 then Result := Indx < SpecCount
  else NotFoundError(QName, QNameLen);
  end;

function TExpatAttributes.IsSpecified(Uri: PSAXChar; UriLen: Integer;
  LocalName: PSAXChar; LocalNameLen: Integer): Boolean;
  var
    Indx: Integer;
  begin
  Indx := GetIndex(Uri, UriLen, LocalName, LocalNameLen);
  if Indx >= 0 then Result := Indx < SpecCount
  else NotFoundError(Uri, UriLen, LocalName, LocalNameLen);
  end;
{$WARNINGS OFF}

procedure TExpatAttributes.CheckIndex(Index: Integer);
  begin
  if (Index < 0) or (Index >= FTotalCount) then
    raise ESAXException.Create(SIndexError);
  end;

procedure TExpatAttributes.NotFoundError(QName: PSAXChar; QNameLen: Integer);
  begin
  raise ESAXException.CreateFmt(SAttributeNotFoundQName,
    [PSAXCharToString(QName, QNameLen)]);
  end;

procedure TExpatAttributes.NotFoundError(Uri: PSAXChar; UriLen: Integer;
  LocalName: PSAXChar; LocalNameLen: Integer);
  begin
  raise ESAXException.CreateFmt(SAttributeNotFoundName,
    [PSAXCharToString(Uri, UriLen), PSAXCharToString(LocalName, LocalNameLen)]);
  end;

{ Notes:
 - the arguments passed are only valid for the current call
 - only global attributes have prefixes                     }
procedure TExpatAttributes.Initialize(ElName: PSAXChar; ElNameLen: Integer;
  const Atts: TAttrs; SpecAttrCount: Integer);
  var
    Indx: Integer;
  begin
  FElement := ElName;  // qualified/literal name of current element
  FElementLen := ElNameLen;
  FAtts := @Atts;
  // each attribute uses two entries in the array: Name, followed by Value
  FSpecCount := SpecAttrCount shr 1;
  Indx := SpecAttrCount;  // offset into first occurrence of default attributes
  while Atts[Indx] <> nil do Inc(Indx);
  FTotalCount := FSpecCount + ((Indx - SpecAttrCount) shr 1);
  end;

{ TExpatParEntDeclHandler }

type
  TExtEntityDecl = record
    PublicId: SAXString;
    SystemId: SAXString;
    Name: SAXString;
    end;
  PExtEntityDecl = ^TExtEntityDecl;

  TExtEntityKey = record
    PublicId: PSAXChar;      // null terminated
    SystemId: PSAXChar;      // null terminated
    end;
  PExtEntityKey = ^TExtEntityKey;

class function TExpatParEntDeclHandler.CompareKey(HashKeyP1, HashKeyP2: Pointer): Boolean;
  var
    KeyP1: PExtEntityKey absolute HashKeyP1;
    KeyP2: PExtEntityKey absolute HashKeyP2;
  begin
  Result := SAXStrEqual(KeyP1.PublicId, KeyP2.PublicId);
  if not Result then Exit;
  Result := SAXStrEqual(KeyP1.SystemId, KeyP2.SystemId);
  end;

constructor TExpatParEntDeclHandler.Create;
  begin
  FFreeItem := FreeParEntDecl;
  end;

procedure TExpatParEntDeclHandler.FreeParEntDecl(ItemP: Pointer);
  begin
  Dispose(PExtEntityDecl(ItemP));
  end;

class function TExpatParEntDeclHandler.GetKey(ItemP: Pointer): Pointer;
  begin
  Result := ItemP;
  end;

function TExpatParEntDeclHandler.Hash(HashKeyP: Pointer): THashValue;
  begin
  with PExtEntityKey(HashKeyP)^ do
    begin
    Result := XMLStrHash2(PublicId, 0);
    Result := XMLStrHash2(SystemId, Result);
    end;
  end;

{ TExpatParEntDecls }

function TExpatParEntDecls.AddDecl(const Name, PublicId, SystemId: SAXString): Boolean;
  var
    EntityDeclP: PExtEntityDecl;
  begin
  New(EntityDeclP);
  EntityDeclP^.PublicId := PublicId;
  EntityDeclP^.SystemId := SystemId;
  EntityDeclP^.Name := Name;
  // should we raise an exception if a duplicate is inserted? //kw
  // if not FHashCursor.Insert(EntityDeclP) then raise ...
  Result := FHashCursor.Add(EntityDeclP);
  if not Result then Dispose(EntityDeclP);
  end;

procedure TExpatParEntDecls.Clear;
  begin
  FHashCursor.Clear;
  end;

constructor TExpatParEntDecls.Create;
  begin
  FEntityHandler := TExpatParEntDeclHandler.Create;
  FEntityTable := TDblHashTable.Create(FEntityHandler);
  FHashCursor := TDblHashTableCursor.Create(FEntityTable);
  end;

function TExpatParEntDecls.DeclExists: Boolean;
  begin
  Result := FHashCursor.Exists;
  end;

destructor TExpatParEntDecls.Destroy;
  begin
  FHashCursor.Free;
  FentityTable.Free;
  FEntityHandler.Free;
  inherited;
  end;

function TExpatParEntDecls.FindDecl(PublicId, SystemId: PSAXChar): Boolean;
  var
    EntityKey: TExtEntityKey;
  begin
  EntityKey.PublicId := PublicId;
  EntityKey.SystemId := SystemId;
  Result := FHashCursor.Locate(@EntityKey);
  end;

procedure TExpatParEntDecls.GetDecl(out Name: SAXString);
  begin
  Name := PExtEntityDecl(FHashCursor.Item)^.Name;
  end;

{ TExpatLocator }

constructor TExpatLocator.Create(Reader: TExpatXMLReader);
  begin
  Assert(Reader <> nil);
  FReader := Reader;
  end;

function TExpatLocator.GetColumnNumber: Integer;
  begin
  with FReader do
    begin
    if FEntityParseData = nil then  // not parsing
      Result := -1
    else with FEntityParseData do
      begin
      Assert(Parser <> nil);
      Result := XMLGetCurrentColumnNumber(Parser);
      end;
    end;
  end;

function TExpatLocator.GetLineNumber: Integer;
  begin
  with FReader do
    begin
    if FEntityParseData = nil then
      Result := -1
    else with FEntityParseData do
      begin
      Assert(Parser <> nil);
      Result := XMLGetCurrentLineNumber(Parser);
      end;
    end;
  end;

// Precondition: see comments to XMLGetInputContext
function TExpatLocator.GetParsePosition: Integer;
  var
    Offset, BufferSize: Integer;
  begin
  with FReader do
    begin
    if FEntityParseData = nil then
      Result := -1
    else with FEntityParseData do
      begin
      if InReader <> nil then
        raise ESAXExpat.Create(SNoPositionForReader);
      Assert(Parser <> nil);
      if XMLGetInputContext(Parser, Offset, BufferSize) <> nil then
        Result := InStream.Position - BufferSize + Offset
      else
        raise ESAXExpat.Create(SCannotGetInputContext);
      end;
    end;
  end;

// returns PublicId of entity currently being parsed - can be external
function TExpatLocator.GetPublicId: PSAXChar;
  begin
  with FReader do
    begin
    if FEntityParseData = nil then  // not parsing
      Result := nil
    else
      Result := PSAXChar(FEntityParseData.PublicId);
    end;
  end;

// returns SystemId of entity currently being parsed - can be external
function TExpatLocator.GetSystemId: PSAXChar;
  begin
  with FReader do
    begin
    if FEntityParseData = nil then
      Result := nil
    else
      //kw must make sure we return an absolute URI
      Result := PSAXChar(FEntityParseData.SystemId);
    end;
  end;

function TExpatLocator.GetEncoding: PSAXChar;
  begin
  with FReader do
    begin
    if FEntityParseData = nil then
      Result := nil
    else
      Result := PSAXChar(FEntityParseData.Encoding);
    end;
  end;

function TExpatLocator.GetXMLVersion: PSAXChar;
  begin
  with FReader do
    begin
    if FEntityParseData = nil then
      Result := nil
    else
      Result := PSAXChar(FEntityParseData.Version);
    end;
  end;

{ TEntityParseData }

procedure TEntityParseData.Clear;
  begin
  FSource := nil;
  FParser := nil;
  FInStream := nil;
  FInReader := nil;
  FName := '';
  FVersion := '';
  FEncoding := '';
  FPublicId := '';
  FSystemId := '';
  end;

procedure TEntityParseData.Init(const Source: IInputSource; IsStreamSource: Boolean;
  Parser: TXMLParser; Name: PSAXChar = nil; NameLen: Integer = 0);
  begin
  FSource := Source;
  FParser := Parser;
  if IsStreamSource then
    begin
    FInStream := IStreamInputSource(Source).ByteStream;
    if FInStream = nil then
      raise ESAXExpat.Create(SInvalidInputStream);
    FInReader := nil;
    end
  else
    begin
    FInReader := IReaderInputSource(Source).Reader;
    if FInReader = nil then
      raise ESAXExpat.Create(SInvalidInputReader);
    FInStream := nil;
    end;
  FName := Name;
  FNameLen := NameLen;
  FEncoding := Source.Encoding;
  FPublicId := Source.PublicId;
  FSystemId := Source.SystemId;
  end;

{ TExpatParseError }

destructor TExpatParseError.Destroy;
  begin
  if FLocator <> nil then FLocator._Release;
  inherited;
  end;

function TExpatParseError.GetMessage: PSAXChar;
  begin
  if FError = nil then
    Result := FMessagePtr
  else
    begin
    FMessageStr := SAXString(FError.Message);
    Result := PSAXChar(FMessageStr);
    end;
  end;

function TExpatParseError.GetNativeError: IUnknown;
  begin
  Result := IExpatParseError(Self);
  end;

procedure TExpatParseError.SetLocator(Value: TExpatLocator);
  begin
  if FLocator <> nil then FLocator._Release;
  FLocator := Value;
  if FLocator <> nil then FLocator._AddRef;
  end;

procedure TExpatParseError.Init(Message: PSAXChar; Locator: TExpatLocator);
  begin
  FError := nil;
  FMessageStr := '';
  FMessagePtr := Message;
  SetLocator(Locator);
  end;

procedure TExpatParseError.Init(Error: Exception; Locator: TExpatLocator);
  begin
  FMessagePtr := nil;
  FError := Error;
  SetLocator(Locator);
  end;

function TExpatParseError.GetError: Exception;
  begin
  Result := FError;
  end;

{ TExpatNativeParseError }

function TExpatNativeParseError.GetNativeError: IUnknown;
  begin
  Result := IExpatNativeParseError(Self);
  end;

function TExpatNativeParseError.GetError: Exception;
  begin
  Result := FError;
  end;

function TExpatNativeParseError.GetLocator: TExpatLocator;
  begin
  Result := FError.Locator;
  end;

function TExpatNativeParseError.GetErrorCode: Integer;
  begin
  Result := ESAXExpatParse(FError).ErrorCode;
  end;

procedure TExpatNativeParseError.Init(Error: ESAXExpatParse);
  begin
  FMessage := '';
  FError := Error;
  end;

function TExpatNativeParseError.GetMessage: PSAXChar;
  begin
  FMessage := SAXString(FError.Message);
  Result := PSAXChar(FMessage);
  end;

{ ESAXExpatParse }

constructor ESAXExpatParse.Create(const Msg: string; Locator: TExpatLocator;
  ErrorCode: Integer);
  begin
  inherited Create(Msg);
  FErrorCode := ErrorCode;
  SetLocator(Locator);
  end;

destructor ESAXExpatParse.Destroy;
  begin
  if FLocator <> nil then FLocator._Release;
  inherited;
  end;

procedure ESAXExpatParse.SetLocator(Value: TExpatLocator);
  begin
  if FLocator <> nil then FLocator._Release;
  FLocator := Value;
  if FLocator <> nil then FLocator._AddRef;
  end;

procedure ESAXExpatParse.Init(const Msg: string; Locator: TExpatLocator;
  ErrorCode: Integer);
  begin
  Message := Msg;
  FErrorCode := ErrorCode;
  SetLocator(Locator);
  end;

{ SAXExpatVendor }

type
  TSAXExpatVendor = class(TBufferedSAXVendor)
    function Description: string; override;
    function XMLReader: IXMLReader; override;
    function BufferedXMLReader: IBufferedXMLReader; override;
  end;

function TSAXExpatVendor.BufferedXMLReader: IBufferedXMLReader;
  begin
  Result := TExpatXMLReader.Create;
  end;

function TSAXExpatVendor.Description: string;
  begin
  Result := XML_SAXVENDOR;
  end;

function TSAXExpatVendor.XMLReader: IXMLReader;
  begin
  Result := nil;
  end;
  
{ TSAXExpat }

function TSAXExpat.GetVendor: string;
  begin
  Result := XML_SAXVENDOR;
  end;

var
  SAXVendor: TSAXVendor;

initialization
  UpdateMemoryManager;

  SAXVendor := TSAXExpatVendor.Create;
  DefaultSAXVendor := SAXVendor.Description;
  RegisterSAXVendor(SAXVendor);

finalization
  UnRegisterSAXVendor(SAXVendor);


end.
