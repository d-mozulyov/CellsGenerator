program DataFiles;

{$APPTYPE CONSOLE}

// compiler options
{$if CompilerVersion >= 24}
  {$LEGACYIFEND ON}
{$ifend}
{$if CompilerVersion >= 23}
  {$define UNITSCOPENAMES}
{$ifend}
{$U-}{$V+}{$B-}{$X+}{$T+}{$P+}{$H+}{$J-}{$Z1}{$A4}
{$ifndef VER140}
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$endif}
{$O+}{$R-}{$I-}{$Q-}{$W-}

{.$define MODECOMPACT}

uses
  {$ifdef UNITSCOPENAMES}
  Winapi.Windows, System.SysUtils, System.Classes, System.Variants,
  System.Generics.Collections,
  {$else}
  Windows, SysUtils, Classes, Variants, Generics.Collections,
  {$endif}
  DocumentData in 'DocumentData.pas',
  Generators in 'Generators.pas',
  XmlGenerators in 'Generators\XmlGenerators.pas',
  Readers in 'Readers.pas',
  XmlReaders in 'Readers\XmlReaders.pas';

// native ordinal types
{$if (not Defined(FPC)) and (CompilerVersion < 22)}
type
  {$if CompilerVersion < 21}
  NativeInt = Integer;
  NativeUInt = Cardinal;
  {$ifend}
  PNativeInt = ^NativeInt;
  PNativeUInt = ^NativeUInt;
{$ifend}

procedure TestDocumentReaders(const DocumentReaders: array of TDocumentReaderClass);
var
  i: Integer;
  DocumentReader: TDocumentReaderClass;
  Generator: TGeneratorClass;
  Instance: TDocumentReader;
  List: TStringList;
  FileName: string;
  Time: Cardinal;
  TestInformation: TTestInformation;
begin
  // check exists binary/text files
  List := TStringList.Create;
  try
    List.Sorted := True;
    List.Duplicates := dupIgnore;

    for i := Low(DocumentReaders) to High(DocumentReaders) do
    begin
      DocumentReader := DocumentReaders[i];

      FileName := DocumentReader.GetDefaultFileName;
      {$ifNdef MODECOMPACT}
      if (not FileExists(FileName)) then
      {$endif}
      begin
        Generator := DocumentReader.GetGenerator;
        List.AddObject(FileName, Pointer(Generator));
      end;
    end;

    if (List.Count <> 0) then
    begin
      {$ifNdef MODECOMPACT}
        Writeln(List.Count, ' files not found.');
        Write('Press Enter to generate them');
        Readln;
      {$endif}

      for i := 0 to List.Count - 1 do
      begin
        FileName := List[i];
        {$ifNdef MODECOMPACT}
          Write('"', ExtractFileName(FileName), '"...');
        {$endif}

        Generator := Pointer(List.Objects[i]);
        try
          with Generator.Create(FileName) do
          try
            Run;
          finally
            Free;
          end;
        except
          Sleep(1000);
          DeleteFile(FileName);
          raise;
        end;

        {$ifNdef MODECOMPACT}
          Writeln(' done.');
        {$endif}
      end;

      {$ifNdef MODECOMPACT}
        Writeln;
      {$endif}
    end;
  finally
    List.Free;
  end;

  Writeln('Correct values: ', CORRECT_TEST_INFORMATION.ToString);
  Writeln;

  // run document readers
  for i := Low(DocumentReaders) to High(DocumentReaders) do
  begin
    DocumentReader := DocumentReaders[i];
    FileName := DocumentReader.GetDefaultFileName;

    Write(DocumentReader.GetDescription, '...');
    Time := GetTickCount;
    Instance := DocumentReader.Create(FileName);
    try
      Instance.Run;
      TestInformation := Instance.TestInformation;
    finally
      Instance.Free;
    end;
    Time := GetTickCount - Time;

    Write(' ', Time, 'ms');
    if (TestInformation.IsCorrect) then
    begin
      Writeln(' done.');
    end else
    begin
      Writeln(' FAILURE.');
      // Writeln('Correct values: ', CORRECT_TEST_INFORMATION.ToString);
      Writeln('Failure values: ', TestInformation.ToString);
    end;
  end;
end;

begin
  try
    TestDocumentReaders([
      TMSSAX60Reader,
      TOXmlSAXReader
      {$ifNdef MODECOMPACT}
      {use dll file}, TExpatReader
      {$endif}
    ]);
  except
    on E: Exception do
    Writeln(E.ClassName, ': ', E.Message);
  end;

  if (ParamStr(1) <> '-nowait') then
  begin
    Writeln;
    Write('Press Enter to quit');
    Readln;
  end;
end.
